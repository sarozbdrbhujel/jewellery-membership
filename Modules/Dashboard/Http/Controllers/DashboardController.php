<?php

namespace Modules\Dashboard\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Customer\Entities\Customer;
use Modules\Inventory\Entities\PurchaseDetails;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $currentDate = \Carbon\Carbon::now();
        // dd($currentDate);
        $agoDate = $currentDate->subDays($currentDate->dayOfWeek)->subWeek();;
    // dd( $agoDate);
        $data['totalAmountToday']=PurchaseDetails::whereDate('created_at', Carbon::today())->sum('amount');
        $data['totalAmount']=PurchaseDetails::sum('amount');
        $data['beforeCustomers']=Customer::where('created_at','<=',$agoDate)->count();
        $data['totalCustomers']=Customer::count();
        $data['percentage']=(Customer::count()!=0)?(($data['totalCustomers']-$data['beforeCustomers'])*100)/$data['totalCustomers']:0;
        // dd($data['beforeCustomers'],$data['totalCustomers']);
        return view('dashboard::newIndex',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('dashboard::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('dashboard::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('dashboard::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
