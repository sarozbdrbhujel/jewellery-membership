<!-- third party js -->
<script src="{{Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/buttons.html5.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/buttons.flash.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/buttons.print.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/datatables/dataTables.select.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{Module::asset('dashboard:libs/pdfmake/vfs_fonts.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<!-- third party js ends -->