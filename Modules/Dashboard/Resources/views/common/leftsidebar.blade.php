<div class="left-side-menu">

    <div class="slimscroll-menu">


        <!--- Sidemenu -->
        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li class="menu-title">Navigation</li>

                <li>
                    <a href="{{ route('dashboard.index') }}">
                        <i class="mdi mdi-view-dashboard"></i>
                        <span> Dashboard </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>
                <li class="menu-title">Customers</li>

                <li>
                    <a href="{{ route('customers.index') }}">
                        <i class="mdi mdi-account-group"></i>
                        <span> Customers </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('gifts.index') }}">
                        <i class="mdi mdi-gift"></i>
                        <span> Customer Gifts </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>
                <li class="menu-title">Inventory</li>
                <li>
                    <a href="{{ route('membership.index') }}">
                        <i class="mdi mdi-cards"></i>
                        <span> Privilege Cards </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('purchase.index') }}">
                        <i class="mdi mdi-baby-carriage"></i>
                        <span> Purchase History</span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>

                <li>
                    <a href="{{ route('cards.index') }}">
                        <i class="mdi mdi-badge-account-horizontal"></i>
                        <span>Members Cards </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('rewards.index') }}">
                        <i class="mdi mdi-trophy"></i>
                        <span>Rewards </span>
                        <span class="menu-arrow"></span>
                    </a>
                </li>

                <li class="menu-title">Marketing and Business</li>
                <li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-bullhorn fa-sm"></i>
                        <span> Marketing</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('customercategories.index') }}">Customer Category</a></li>
                        <li><a href="{{ route('marketing.officer') }}">Marketing Officer</a></li>
                        <li><a href="{{ route('customers.email') }}">Compose Mail</a></li>
                    </ul>
                </li>
                </li>

                <li class="menu-title">Messages And Wish</li>
                <li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="mdi mdi-message-draw"></i>
                        <span> Message </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="{{ route('messages.index') }}">Wish Messages</a></li>
                    </ul>
                </li>
                </li>

            </ul>

        </div>
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
