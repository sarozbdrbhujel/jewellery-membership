        <!-- App favicon -->
        <link rel="shortcut icon" href="{{Module::asset('dashboard:images/fevicon.ico')}}">

        <!-- Bootstrap Css -->
        <link href="{{Module::asset('dashboard:css/bootstrap.min.css')}}" id="bootstrap-stylesheet" rel="stylesheet" type="text/css" />
        <!-- Icons Css -->
        <link href="{{Module::asset('dashboard:css/icons.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- App Css-->
        <link href="{{Module::asset('dashboard:css/app.min.css')}}" id="app-stylesheet" rel="stylesheet" type="text/css" />
        <link href="{{Module::asset('dashboard:css/custom.css')}}" id="app-stylesheet" rel="stylesheet" type="text/css" />
        <style>
                 .se-pre-con {
            position: fixed;
            left: 0px;
            top: 0px;
            width: auto;
            height: auto;
            /* width: 100%;
            height: 100%; */
            z-index: 9999;
            /* background-color: black; */
            /* background: url('https://smallenvelop.com/wp-content/uploads/2014/08/Preloader_11.gif') center no-repeat #fff; */
            background: black url(../modules/dashboard/images/loading.gif) center no-repeat;
            /* C:\xampp\htdocs\jewellery-master\public\assets\images\JOSWhiteGIF.gif */
            background-size: 50% 50%;
            /* overflow-y: hidden; */
            }

        </style>
        @yield('style')