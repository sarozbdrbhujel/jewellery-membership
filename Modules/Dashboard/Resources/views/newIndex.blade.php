@extends('dashboard::newLayouts.master')
@section('title')
    Dashboard || Shree Ganapati Jewellers
@endsection
@section('content')
<div class="layout-content">

    <!-- [ content ] Start -->
    <div class="container-fluid flex-grow-1 container-p-y">
        <h4 class="font-weight-bold py-3 mb-0">Dashboard</h4>
        
        <div class="row">
            <!-- 1st row Start -->
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="">
                                        <h2 class="mb-2"> Rs. {{$totalAmountToday}} </h2>
                                        <p class="text-muted mb-0"><span class="badge badge-primary">Revenue</span> Today</p>
                                    </div>
                                    <div class="lnr lnr-leaf display-4 text-primary"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="">
                                        <h2 class="mb-2">Rs.{{$totalAmount}} </h2>
                                        <p class="text-muted mb-0"><span class="badge badge-success"></span>Total Revenue</p>
                                    </div>
                                    <div class="lnr lnr-chart-bars display-4 text-success"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="">
                                        <h2 class="mb-2">{{$totalCustomers}}</h2>
                                        <p class="text-muted mb-0"><span class="badge badge-warning"></span> Total Customers</p>
                                    </div>
                                    <div class="lnr lnr-cart display-4 text-warning"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card mb-4">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-between">
                                    <div class="">
                                        <h2 class="mb-2"> {{$totalCustomers-$beforeCustomers}} <small></small></h2>
                                        <p class="text-muted mb-0">New <span class="badge badge-danger">{{round($percentage,0)}}%</span> Customer</p>
                                    </div>
                                    <div class="lnr lnr-rocket display-4 text-danger"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
           
            <!-- 1st row Start -->
        </div>
        
    
    </div>
    <!-- [ content ] End -->

</div>
@endsection
@section('script')
<script src="{{Module::asset('dashboard:assets/libs/chart-am4/core.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/libs/chart-am4/charts.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/libs/chart-am4/animated.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/libs/eve/eve.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/libs/flot/flot.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/libs/flot/curvedLines.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/pages/dashboards_index.js')}}"></script>
@endsection