<!-- Icon fonts -->
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/fontawesome.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/ionicons.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/linearicons.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/open-iconic.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/pe-icon-7-stroke.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/feather.css')}}">

<!-- Core stylesheets -->
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/bootstrap-material.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/shreerang-material.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/uikit.css')}}">

<!-- Libs -->
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/libs/perfect-scrollbar/perfect-scrollbar.css')}}">
<link rel="stylesheet" href="{{Module::asset('dashboard:assets/libs/flot/flot.css')}}">
<link href="{{Module::asset('dashboard:css/custom.css')}}" id="app-stylesheet" rel="stylesheet" type="text/css" />
<style>
 .se-pre-con {
     position: fixed;
     left: 0px;
     top: 0px;
     width: 100%;
     height: 100%;
     z-index: 9999;
     /* background-color: black; */
     /* background: url('https://smallenvelop.com/wp-content/uploads/2014/08/Preloader_11.gif') center no-repeat #fff; */
     background: #b7ddb9  url(/modules/dashboard/images/loading.gif) center no-repeat;
     background-size: 30px 15px;
     animation: rotation 2s infinite linear;
    }
.btn-purple{
    background-color: blueviolet
}
.select2-container{
    width: 90% !important;
}
</style>
@yield('style')