<div id="layout-sidenav" class="layout-sidenav sidenav sidenav-vertical bg-white logo-dark">
    <!-- Brand demo (see assets/css/demo/demo.css) -->
    <div class="app-brand demo">
        <span class="app-brand-logo demo">
            <img src="{{Module::asset('dashboard:images/logo.png')}}" alt="Brand Logo" class="img-fluid logoImg">
        </span>
        <a href="index.html" class="app-brand-text demo sidenav-text font-weight-normal ml-2">Empire</a>
        <a href="javascript:" class="layout-sidenav-toggle sidenav-link text-large ml-auto">
            <i class="ion ion-md-menu align-middle"></i>
        </a>
    </div>
    <div class="sidenav-divider mt-0"></div>

    <!-- Links -->
    <ul class="sidenav-inner py-1">

        <!-- Dashboards -->
        
        <li class="sidenav-item active">
            <a href="{{ route('dashboard.index') }}" class="sidenav-link">
                <i class="sidenav-icon feather icon-home"></i>
                <div>Dashboards</div>
                {{-- <div class="pl-1 ml-auto">
                    <div class="badge badge-danger">Hot</div>
                </div> --}}
            </a>
        </li>

        <!-- Layouts -->
        <li class="sidenav-divider mb-1"></li>
        <li class="sidenav-header small font-weight-semibold">Customers</li>
        
        <li class="sidenav-item">
            <a  href="{{ route('customers.index') }}" class="sidenav-link">
                <i class="sidenav-icon feather icon-users"></i>
                <div>Customers</div>
            </a>
        </li>
        <li class="sidenav-item">
            <a  href="{{ route('gifts.index') }}" class="sidenav-link">
                <i class=" sidenav-icon lnr lnr-gift"></i>
                <div>Gifts</div>
            </a>
        </li>
        <li class="sidenav-divider mb-1"></li>
        <li class="sidenav-header small font-weight-semibold">Inventory</li>
        <li class="sidenav-item">
            <a href="javascript:" class="sidenav-link sidenav-toggle">
                <i class="sidenav-icon lnr lnr-cart"></i>
                <div>Purchase </div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('purchase.index') }}" class="sidenav-link">
                        <div>Purchase History</div>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{ route('purchase.create') }}" class="sidenav-link">
                        <div>Purchase Create</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidenav-item">
            <a  href="{{ route('membership.index') }}" class="sidenav-link">
                <i class="sidenav-icon lnr lnr-picture"></i>
                <div>Privilege Cards</div>
            </a>
        </li>
       
        
        <li class="sidenav-item">
            <a  href="{{ route('cards.index') }}" class="sidenav-link">
                <i class=" sidenav-icon lnr lnr-license"></i>
                <div>Members Cards</div>
            </a>
        </li>
        <li class="sidenav-item">
            <a  href="{{ route('rewards.index') }}" class="sidenav-link">
                <i class=" sidenav-icon lnr lnr-gift"></i>
                <div>Rewards</div>
            </a>
        </li>
        

      

        

        <!--  Icons -->
        <li class="sidenav-divider mb-1"></li>
        <li class="sidenav-header small font-weight-semibold">Marketing and Business</li>
        <li class="sidenav-item">
            <a href="javascript:" class="sidenav-link sidenav-toggle">
                <i class="sidenav-icon lnr lnr-rocket"></i>
                <div>Marketing</div>
            </a>
            <ul class="sidenav-menu">
                <li class="sidenav-item">
                    <a href="{{ route('customercategories.index') }}" class="sidenav-link">
                        <div>Customer Category</div>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{ route('marketing.officer') }}" class="sidenav-link">
                        <div>Marketing Officer</div>
                    </a>
                </li>
                <li class="sidenav-item">
                    <a href="{{ route('customers.email') }}" class="sidenav-link">
                        <div>Compose Mail</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidenav-item">
            <a  href="{{ route('messages.index') }}" class="sidenav-link">
                <i class=" sidenav-icon feather icon-mail"></i>
                <div>Email Templates</div>
            </a>
        </li>
        <li class="sidenav-header small font-weight-semibold">Customers Import</li>
        <li class="sidenav-item">
            <a  href="{{ route('imports.get') }}" class="sidenav-link">
                <i class=" sidenav-icon lnr lnr-gift"></i>
                <div>Customers Import</div>
            </a>
        </li>
    </ul>
</div>