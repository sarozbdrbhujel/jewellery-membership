<!-- Core scripts -->
<script src="{{Module::asset('dashboard:assets/js/pace.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/jquery-3.2.1.min.js')}}"></script>
{{-- <script src="{{Module::asset('dashboard:js/jquery.min.js')}}"></script> --}}
<script src="{{Module::asset('dashboard:assets/libs/popper/popper.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/bootstrap.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/sidenav.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/layout-helpers.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/material-ripple.js')}}"></script>

<!-- Libs -->
<script src="{{Module::asset('dashboard:assets/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>



<!-- Demo -->
<script src="{{Module::asset('dashboard:assets/js/demo.js')}}"></script>
<script src="{{Module::asset('dashboard:assets/js/analytics.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/parsley.js/2.9.2/parsley.min.js" integrity="sha512-eyHL1atYNycXNXZMDndxrDhNAegH2BDWt1TmkXJPoGf1WLlNYt08CSjkqF5lnCRmdm3IrkHid8s2jOUY4NIZVQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
 
  $(document).ready(function(){
    $('#formValidation').parsley();
    
    
        console.log('page is loaded');
  });
  $(window).on('load',function(){
			console.log('loaded')
			// Animate loader off screen
			// setInterval()
			// setInterval(function() {
			//     $(".se-pre-con").fadeOut('slow');
			//   }, 2000);
			$(".se-pre-con").fadeOut();
			
		});
  </script>
@yield('script')