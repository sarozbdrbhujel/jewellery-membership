<?php

namespace Modules\Marketing\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Console\Scheduling\Schedule;

class ScheduleServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booted(function () {
            // $schedule = $this->app->make(Schedule::class);
            // $schedule->command('birthday:wish')->everyMinute()
            // ->appendOutputTo (storage_path().'/logs/laravel_schedule.log');

            // $schedule->command('queue:work')->everyMinute()
            // ->appendOutputTo (storage_path().'/logs/laravel_schedule.log');

        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
