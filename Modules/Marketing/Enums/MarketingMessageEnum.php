<?php

namespace Modules\Marketing\Enums;

use BenSampo\Enum\Enum;

class MarketingMessageEnum extends Enum
{
    const SMSBirthdayWish = "SMS Birthday Wish ";
    const EmailBirthdayWish = "Email Birthday Wish ";
    const SMSAnniversarydayWish = "SMS Anniversary Wish ";
    const EmailAnniversarydayWish = "Email Anniversary Wish ";
    const EmailWelcomeWish = "Email Welcome Wish ";
    const SMSWelcomeWish = "SMS Welcome Wish ";
    const SMSSpecialWish = "SMS Special Wish ";
    const EmailSpecialWish = "Email Special Wish ";
}
