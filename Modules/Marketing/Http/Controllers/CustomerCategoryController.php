<?php

namespace Modules\Marketing\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
// use Modules\AccessManagement\Enums\ModulesEnum;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Marketing\Entities\ClientCategory;

class CustomerCategoryController extends Controller
{

    public function __construct(ClientCategory $clientCategory, CustomerInterface $employeeInterface)
    {
        $this->clientCategory =$clientCategory;
        $this->employeeInterface = $employeeInterface;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data['clientCategories'] = $this->clientCategory->get();
        return view('marketing::customercategory.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('marketing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        try
        {
            $this->clientCategory->create($request->all());

        } catch(Exception $e)
        {
            dd($e->getMessage());
        }
        return redirect()->route('customercategories.index')->with('message', 'successfully create a customer category');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('marketing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('marketing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        try
        {
            $category = $this->clientCategory->findOrFail($id);
            $category->update($request->all());

        } catch(Exception $e)
        {
            dd($e->getMessage());
        }
        return redirect()->route('customercategories.index')->with('message', 'successfully update a customer category');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        $category = $this->clientCategory->findOrFail($id)->delete();
        return back()->with('message', 'successfully delete a customer category from records');
    }
    public function linkCustomers($id)
    {
        $data['customers'] = $this->employeeInterface->getAll();
        // dd( $data['customers']);
        $data['customerCategories'] = $this->clientCategory->with('accountUsers')->findOrFail($id);
        // dd($data['customerCategories']);

        // dd($data);
        return view('marketing::customercategory.linkcustomer', $data);
    }

    public function postLinkCustomers(Request $request, $id)
    {
        // DD($request->all(),$id);
        $category = $this->clientCategory->findOrFail($id);
        $category->accountUsers()->sync($request->customer_id);
        return back()->with('message', 'Customer Grouped to Category Successfully');
        // dd($request->all());

    }
}
