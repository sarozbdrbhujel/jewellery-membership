<?php

namespace Modules\Marketing\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Marketing\Entities\Message;

class MessageController extends Controller
{

    public function __construct(Message $message)
    {
        $this->message = $message ;
    }
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $data['messages'] =  $this->message->get();
        return view('marketing::message.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('marketing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        $message = $this->message->create($request->all());
        return redirect()->route('messages.index')->with('message', 'successfully add a message');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('marketing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('marketing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try
        {
            DB::beginTransaction();
            $message = $this->message->findOrFail($id);
            $updateb = $message->update($request->all());
            // dd($updateb);

        }catch(Exception $e)
        {
            DB::rollback();
            dd($e->getMessage());
        }
        DB::commit();
        return redirect()->route('messages.index')->with('message', 'successfully update a message item ');

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // dd('sdsdf');
        $this->message->findOrfail($id)->delete();
        return back()->with('message' , 'successfully deleted a message item from record');
    }
}
