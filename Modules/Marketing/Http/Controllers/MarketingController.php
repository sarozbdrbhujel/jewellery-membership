<?php

namespace Modules\Marketing\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Marketing\Entities\MarketingEmail;

class MarketingController extends Controller
{

    public function __construct(MarketingEmail $marketingEmail)
    {
        $this->marketingEmail = $marketingEmail;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('marketing::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('marketing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('marketing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('marketing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function marketingOfficer()
    {

        $data['marketingOfficer'] = $this->marketingEmail->get();
        return view('marketing::marketingofficer.index', $data);

    }

    public function marketingOfficerStore(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $data = [
                'name' => $request->name,
                'email_address' => $request->email_address,

            ];
           $client =  $this->marketingEmail->create($data);



        }catch(Exception $e)
        {
            dd($e->getMessage());
            DB::rollback();

        }
        DB::commit();
        return redirect()->route('marketing.officer')->with('message', 'Successfully Added a Marketing Officer');

    }
    public function marketingOfficerUpdate(Request $request, $id)
    {
        try
        {
           $marketingEmail =  $this->marketingEmail->find($id);
            DB::beginTransaction();
            $data = [
                'name' => $request->name,
                'email_address' => $request->email_address,

            ];
           $client =  $marketingEmail->update($data);



        }catch(Exception $e)
        {
            dd($e->getMessage());
            DB::rollback();

        }
        DB::commit();
        return redirect()->route('marketing.officer')->with('message', 'Successfully Update a Marketing Officer');
    }

    public function marketingOfficerDelete($id)
    {
        $marketingEmail =  $this->marketingEmail->findOrFail($id);
        $marketingEmail->delete();
        return back()->with('message', 'Successfully Deleted a Marketing Officer');

    }
}
