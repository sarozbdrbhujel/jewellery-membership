<?php

namespace Modules\Marketing\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\AccessManagement\Entities\Role;
use Modules\AccessManagement\Enums\ModulesEnum;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Marketing\Entities\ClientCategory;
use Modules\Marketing\Entities\MarketingEmail;
use Modules\Marketing\Jobs\CustomEmailJob;
use Modules\Marketing\Jobs\SMSEmailJob;
use Sparrow;

// use Modules\AccessManagement\Enums\ModulesEnum;

class CustomerController extends Controller
{

    public function __construct(
        CustomerInterface $employeeInterface, MarketingEmail $marketingEmail,
        ClientCategory $clientCategory
    )
    {
        $this->employeeInterface = $employeeInterface ;
        $this->marketingEmail = $marketingEmail ;
        $this->clientCategory = $clientCategory ;

    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        // dd($this->employeeInterface);
        $data['clientCategories'] = $this->clientCategory->get();
        $data['customers'] = $this->employeeInterface->account->whereHas('roleType' , function($query){
                                $query->where('name', ModulesEnum::Client);
        } )->with('clientCategories')->get();
        // dd($data);
        // return view('humanresource::employee.index', $data);
        return view('marketing::customer.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('marketing::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request, Role $role)
    {
        // dd($request->all());

        $findRole = $role->where('name', ModulesEnum::Client)->first();
        // dd($findRole);
        try
        {
            DB::beginTransaction();
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'usertype_id' => $findRole->id,
            ];
           $client =  $this->employeeInterface->store($data);

        //    dd($client);
           $clientDetail = $client->userDetail()->create([
               'join_date' => $request->join_date,
               'leave_date' => $request->expire_date,
               'permanent_address' => $request->address,
               'temporary_address' => $request->temporary_address,
               'phone' => $request->phone,
               'designation' => $request->designation,
               'state' => $request->state,
               'dob' => $request->dob,
               'merriage_anniversary' => $request->anniversary,
           ]);
        //    dd($clientDetail);
        $client->clientCategories()->sync($request->client_category_id);

        }catch(Exception $e)
        {
            dd($e->getMessage());
            DB::rollback();

        }
        DB::commit();
        return redirect()->route('customers.index')->with('message', 'Successfully Added a Client');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('marketing::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('marketing::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id, Role $role)
    {
        // dd($request->all());
        $findRole = $role->where('name', ModulesEnum::Client)->first();
        // dd($findRole);
        try
        {
            DB::beginTransaction();
            $data = [
                'name' => $request->name,
                'email' => $request->email,
                'usertype_id' => $findRole->id,
            ];
            $employeeInterface = $this->employeeInterface->findOrFail($id);
            // dd($employeeInterface);
            $client =  $employeeInterface->update($data);

            $employeeInterface->clientCategories()->sync($request->client_category_id);


        $details = $employeeInterface->userDetail()->first();
        // dd($details);
           $clientDetail = $details->update([
               'join_date' => $request->join_date,
               'leave_date' => $request->expire_date,
               'permanent_address' => $request->address,
               'temporary_address' => $request->temporary_address,
               'phone' => $request->phone,
               'designation' => $request->designation,
               'state' => $request->state,
               'dob' => $request->dob,
               'merriage_anniversary' => $request->anniversary,
           ]);
        //    dd($clientDetail);
        }catch(Exception $e)
        {
            dd($e->getMessage());
            DB::rollback();

        }
        DB::commit();
        return redirect()->route('customers.index')->with('message', 'Successfully update a Client');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $employeeInterface = $this->employeeInterface->findOrFail($id)->delete();
        return back()->with('message', 'Successfully deleted a Client from record');
    }

    public function emailTemplate()
    {
        // dd('fdgfgdf');
        $data['customers'] = $this->employeeInterface->getAll();
        $data['clientCategory'] = $this->clientCategory->get();
        // dd($data);
        $data['marketingOfficers'] = $this->marketingEmail->get();
        return view('marketing::customer.email', $data);
        // return view('marketing::customer.editor');
    }

    public function sendCustomeBulkEmail(Request $request)
    {
        // dd($request->all());
        $pureText = strip_tags(preg_replace('/\s+/', '', $request->tm));
        // $this->sendSms($request->all());
        // dd($request->tm, strip_tags($request->tm, '&nbsp;'));
        // dd($request->tm,$pureText);
        $minute = 0;
        if ($request->time)
        {
            $now = Carbon::now();
            $sendlater = $request->time;
            $carbonDate = Carbon::parse($sendlater);
            // dd($now, $carbonDate);
            $minute = $now->diffInMinutes($carbonDate);
            // dd($now, $carbonDate, $minute);


        }

        if($request->has('send_copy'))
        {
            $this->sendCopyToME($request, $minute);
        }

        /**
         * please note that this method was modified to individual user
         * client to group category client so that following code may be
         * used in lated , all the commented code was previous code
         * extra loop added was foreach($request->client_category_id as $clientCategoryId)
         * extra condition was if($category->accountUsers->isNotEmpty())
         */
        foreach($request->client_category_id as $clientCategoryId)
        {
            $category = $this->clientCategory->with('accountUsers')->findOrFail($clientCategoryId);
            // dump($category);
            if($category->accountUsers->isNotEmpty())
            {
                // dd($request->all());
                // foreach ($request->custmers as $customer)
                foreach ($category->accountUsers as $client)
                {

                    // $client =  $this->employeeInterface->account->findOrFail($customer);
                    
                    // if($request->sendtype == 'both' || $request->sendtype == 'email' && !is_null($client->email))
                    // {
                    //     dispatch((new CustomEmailJob($request->marketingofficer, $client->email, $request->tm, $request->subject, $client->name))->delay(Carbon::now()->addMinutes($minute)));
                    // }
                    // dd($client);
                    // dd($request->sendtype,$client);
                    if($request->sendtype == 'both' || $request->sendtype == 'sms' && !is_null($client->phone))
                    {
                        // dd('sms');
                        // dd($client->phone);
                        dispatch((new SMSEmailJob($request->marketingofficer, $client->phone,  strip_tags($request->sms_text, '&nbsp;'), $client->name))->delay(Carbon::now()->addMinutes($minute)));
                    }

                }
            }


        }

        // foreach ($request->custmers as $customer)
        // {

        //     $client =  $this->employeeInterface->account->findOrFail($customer);

        //     if($request->sendtype == 'both' || $request->sendtype == 'email' )
        //     {
        //         dispatch((new CustomEmailJob($request->marketingofficer, $client->email, $request->tm, $request->subject, $client->name))->delay(Carbon::now()->addMinutes($minute)));
        //     }
        //     if($request->sendtype == 'both' || $request->sendtype == 'sms' )
        //     {

        //         dispatch((new SMSEmailJob($request->marketingofficer, $client->userDetail->first()->phone,  strip_tags($request->tm, '&nbsp;'), $client->name))->delay(Carbon::now()->addMinutes($minute)));
        //     }

        // }
        return back()->with('message', 'successfully send email to clients');

    }

    public function sendCopyToME($request, $minute)
    {
        $client = $this->shopInterface->getModel()->first();
        // dd($client);

        if($request->sendtype == 'both' || $request->sendtype == 'email' )
        {
            dispatch((new CustomEmailJob($request->marketingofficer, $client->email, $request->tm, $request->subject, $client->shop_name))->delay(Carbon::now()->addMinutes($minute)));
        }
        if($request->sendtype == 'both' || $request->sendtype == 'sms' )
        {

            dispatch((new SMSEmailJob($request->marketingofficer, $client->phone,  strip_tags($request->sms_text, '&nbsp;'), $client->name))->delay(Carbon::now()->addMinutes($minute)));
        }

    }

    public function sendSms($request)
    {
        $from = 'InfoSms'; // Setting FROM provided by Sparrow
        $access_token = 'gZ61TJVlxRdak44wcEix'; // Setting Access Token provided by Sparrow

        $to = '9846579872'; // Setting Phone Number
        $message = 'Hey This is Send From Local Server.'; // Setting Message

        // Send the message
        // dd('sparrow sms');
        // $sms_message = Sparrow::send($to, $message, $from, $access_token);
        // This will return a pseudo JSON response, you will need to json_decode it.
        //  dd($sms_message);

         $args = http_build_query(array( 'token' => 'gZ61TJVlxRdak44wcEix', 'from' => 'Ganapati', 'to' => '9846579872', 'text' => 'HELO!! TESTING Sprrow SMS'));

         $url = "http://api.sparrowsms.com/v2/sms/";

         # Make the call using API.

         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, $url);

         curl_setopt($ch, CURLOPT_POST, 1);

         curl_setopt($ch, CURLOPT_POSTFIELDS,$args);

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //  Response $response = curl_exec($ch);
        $response = curl_exec($ch);

         $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

         curl_close($ch);
    }
}
