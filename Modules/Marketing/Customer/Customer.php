<?php
namespace Modules\Marketing\Customer;

use Carbon\Carbon;
use Modules\AccessManagement\Enums\ModulesEnum;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Marketing\Entities\Message;
use Modules\Marketing\Enums\MarketingMessageEnum;
use Modules\Marketing\Jobs\BirthdayWishJob;
use Modules\Marketing\Jobs\SMSEmailJob;

class Customer
{
    public function __construct(CustomerInterface $employeeInterface, Message $message)
    {
        $this->employeeInterface = $employeeInterface ;
        $this->message = $message ;
    }

    public function customerList()
    {
       $customers =  $this->employeeInterface->account->whereHas('roleType' , function($query){
            $query->where('name', ModulesEnum::Client);
            } )->with('clientCategories')->get();
        $emailBirthdayWishish = $this->message
                                ->where('message_type', MarketingMessageEnum::EmailBirthdayWish)
                                ->first();
        $smsBirthdayWishish = $this->message
                                ->where('message_type', MarketingMessageEnum::SMSBirthdayWish)
                                ->first();
        $smsAnniversarydayWish = $this->message
                                ->where('message_type', MarketingMessageEnum::SMSAnniversarydayWish)
                                ->first();
        $emailAnniversarydayWish = $this->message
                                ->where('message_type', MarketingMessageEnum::EmailAnniversarydayWish)
                                ->first();
        // dd($emailBirthdayWishish, $emailAnniversarydayWish );



        $birthdayWishTitle = "Happy birthday! I hope all your birthday wishes and dreams come true.";
        $anniversartWishTitle = "Happy anniversary! I hope all your anniversary wishes and dreams come true.";
        $minute =  0;
        foreach ($customers as $client)
        {
            // $client->userDetail->first()->dob;
            if(!is_null($client->userDetail->first()->dob) && $client->userDetail->first()->dob->format('m-d')  == Carbon::now()->format('m-d'))
            // if(!is_null($client->userDetail->first()->dob))
            {
                // dd($client->email,$client->userDetail->first()->dob->format('m-d'), Carbon::now()->format('Y-m-d'));
                  dispatch((new BirthdayWishJob('santoshghimire332211@gmail.com', $client->email, $emailBirthdayWishish->message, $birthdayWishTitle, $client->name))->delay(Carbon::now()->addMinutes($minute)));
                  /**
                 * this job work for dispatching work of anniversary sms message to client
                 */
                //   dispatch((new SMSEmailJob('santoshghimire332211@gmail.com', $client->userDetail->first()->phone, strip_tags($smsBirthdayWishish->message, '&nbsp;'), $client->name))->delay(Carbon::now()->addMinutes($minute)));
            }
            if(!is_null($client->userDetail->first()->dob) && $client->userDetail->first()->merriage_anniversary->format('m-d')  == Carbon::now()->format('m-d'))
            // if(!is_null($client->userDetail->first()->dob))
            {
                  dispatch((new BirthdayWishJob('santoshghimire332211@gmail.com', $client->email, $emailAnniversarydayWish->message, $anniversartWishTitle, $client->name))->delay(Carbon::now()->addMinutes($minute)));
                /**
                 * this job work for dispatching work of anniversary sms message to client
                 */
                //   dispatch((new SMSEmailJob('santoshghimire332211@gmail.com', $client->userDetail->first()->phone, strip_tags($smsAnniversarydayWish->message, '&nbsp;'), $client->name))->delay(Carbon::now()->addMinutes($minute)));
            }

        }
    }
}
