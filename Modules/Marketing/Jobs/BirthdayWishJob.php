<?php

namespace Modules\Marketing\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use Modules\Marketing\Emails\BirthdayWishMail;

class BirthdayWishJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $fromAddress,  $customer, $template, $subject, $username;
    public function __construct($fromAddress, $customer, $template, $subject, $username)
    {
        $this->customer = $customer;
        $this->template = $template;
        $this->fromAddress = $fromAddress;
        $this->subject = $subject;
        $this->username = $username;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $custmoEmail = new BirthdayWishMail($this->fromAddress, $this->template, $this->subject, $this->username);
        Mail::to($this->customer)->send($custmoEmail);
    }
}
