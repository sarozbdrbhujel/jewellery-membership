<?php

namespace Modules\Marketing\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SMSEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $officerName , $phone, $message, $clientName;
    public function __construct($officerName, $phone, $message, $clientName)
    {
        $this->officerName = $officerName ;
        $this->phone = $phone ;
        $this->message = $message ;
        $this->clientName = $clientName ;
        // dd('dgdfdgv');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // dd($this,'handle');
        $text = 'Dear '.$this->clientName. ' '.$this->message ;
        // dd($text);
        $args = http_build_query(array( 'token' => 'v2_HJ06k52iWEcc8lG6gNVOrTaAhH7.pR9i', 'from' => 'Ganapati', 'to' => $this->phone, 'text' => $text));

         $url = "http://api.sparrowsms.com/v2/sms/";

         # Make the call using API.

         $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, $url);

         curl_setopt($ch, CURLOPT_POST, 1);

         curl_setopt($ch, CURLOPT_POSTFIELDS,$args);

         curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //  Response $response = curl_exec($ch);
        $response = curl_exec($ch);

         $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

         curl_close($ch);
    }
}
