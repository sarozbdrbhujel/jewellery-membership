<?php

namespace Modules\Marketing\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BirthdayMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $subject;
    public $emailMessage;
    public $user;

    public function __construct($subject,$emailMessage,$user)
    {
        $this->subject = $subject;
        $this->emailMessage = $emailMessage;
        $this->user = $user;
    }

    /**
     * Build the emailMessage.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
                    ->view('marketing::emails.emailMessages')->with(['name'=>$this->user,'emailMessage'=>$this->emailMessage]);
    }
}
