<?php

namespace Modules\Marketing\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BirthdayWishMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    private $fromAddress, $template;
    public $subject;
    public function __construct($fromAddress, $template, $subject, $username)
    {
        $this->template = $template;
        $this->fromAddress = $fromAddress;
        $this->subject = $subject;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        // ->from($this->fromAddress)
        ->subject($this->subject)
        ->view('marketing::emails.notificationemail')->with(['template' =>  $this->template, 'username' => $this->username]);
    }
}
