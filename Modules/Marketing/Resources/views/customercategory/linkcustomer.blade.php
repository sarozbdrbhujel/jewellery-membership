@extends('dashboard::newLayouts.master')
@section('title')
    Customer Category|| Add Customers
@endsection
@section('style')
    {{-- <link href="{{ Module::asset('dashboard:libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
<link href="{{ Module::asset('dashboard:libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h4 class="mt-0 header-title">{{ $customerCategories->category_name }}
                        {{-- <a href="#custom-modal" class="btn btn-purple btn-rounded w-md waves-effect waves-light mb-3 float-right"  data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="mdi mdi-plus"></i> Add Clients to Category</a> --}}
                        <a class="btn btn-purple btn-rounded waves-effect waves-light mb-3 float-right" data-toggle="modal"
                            id="modelBtnClientCategory"><i class="mdi mdi-plus"></i> Add Clients to Category</a>
                    </h4>
                    <div class="table-responsive">
                        <table class="table table-centered mb-0" id="inline-editable">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Customer Name</th>
                                    <th>Status</th>
                                    <th>Action</th>


                                </tr>
                            </thead>

                            <tbody>

                                @forelse ($customerCategories->accountUsers as $key => $tax)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>
                                            {{ $tax->name }}
                                        </td>
                                        <td>
                                            <ul>
                                                {{-- @forelse ($tax->payrolls as $payroll)
                                                <li>
                                                    {{ $payroll->payroll_name }}
                                                </li>
                                            @empty

                                            @endforelse --}}

                                            </ul>
                                        </td>
                                        <td>
                                            {{-- <a class="btn btn-info btn-sm " href="{{ route('taxes.edit', $tax->id) }}">
                                            <i class="fa fa-edit">
                                            </i>
                                        </a> --}}
                                            &nbsp; &nbsp;
                                        </td>
                                    </tr>
                                @empty
                                @endforelse



                            </tbody>
                        </table>
                    </div> <!-- end .table-responsive-->
                </div> <!-- end card-body -->
            </div> <!-- end card -->
        </div> <!-- end col -->


    </div> <!-- end row -->
    <!-- Modal -->
   
    <div class="modal fade gift-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="modelClientCategory">

        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Select a Customers (client ) from dropdown</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                
                <form method="POST"
                    action="{{ route('post.linkcustomer.customercategories', [$customerCategories->id]) }}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="name"> Choose Customers</label>
                            <select class="select2 select2-multiple js-example-basic-multiple" name="customer_id[]"
                                multiple="multiple" required multiple data-placeholder="Choose ...">
                                @forelse ($customers as $payroll)
                                {{-- @dd($payroll) --}}
                                    <option value="{{ $payroll->id }}"> {{ $payroll->name }} </option>
                                @empty
                                @endforelse
                            </select>
                            <div class="form-group">
                                <label for="">Or By Age</label>::
                                <div class="input-group">
                                    &nbsp;{!! Form::label('from', 'From') !!} &nbsp;
                                    {!! Form::text('from_age', null, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'fromDate',
                                        'placeholder' => 'Form Age',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>

                                <div class="input-group">
                                    {!! Form::label('to', 'To') !!} &nbsp;
                                    {!! Form::text('to_age', null, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'toDate',
                                        'placeholder' => 'To Age',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <input type="checkbox" class="" checked parsley-trigger="change" required
                                placeholder="Enter Tax Heading">
                        </div> --}}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="giftBtn">Submit</button>
                    </div>

                </form>
            </div>
        </div>
    </div>


    </div> <!-- end row -->
    <!-- Modal -->



@endsection

@section('script')
    {{-- <script src="{{ Module::asset('dashboard:libs/multiselect/jquery.multi-select.js')}}"></script> --}}
    <script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> --}}
    <script>
        $(document).ready(function() {
            @if (Session::has('message'))
                //  toastr.info("Have fun storming the castle!")
                toastr.success('{{ Session::get('message') }}')
            @endif

            // Instantiate new modal
            // var modal = new Custombox.modal({
            // content: {
            //     effect: 'fadein',
            //     target: '#custom-modal'
            // }
            // });

            // // Open
            // modal.open();

            $(document).on('click', '#modelBtnClientCategory', function() {

                $('#modelClientCategory').modal('show');
            })

            $('.js-example-basic-multiple').select2({
                dropdownParent: $('#modelClientCategory')
            });


            // $('.js-example-basic-multiple').select2();
        });
        // 
    </script>
@stop
