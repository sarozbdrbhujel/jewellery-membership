@extends('dashboard::newLayouts.master')
@section('title')
    Customer Cateory Lists
@endsection
@section('style')
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
type="text/css" />
@endsection
@section('content')

    <div class="row">
        <div class="col-xl-9">
            <div class="card-box">
                <h4 class="header-title mt-0 mb-3">Client Category List
                    <a href="#custom-modal" class="btn btn-purple btn-rounded w-md waves-effect waves-light mb-3 float-right"
                        data-animation="fadein"  id="customerCategoryBtn"><i class="mdi mdi-plus"></i> Create Client Category</a>
                </h4>


                <div class="table-responsive">
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Category Name</th>
                                <th>Description</th>
                                <th> Created</th>
                                <th>Status</th>
                                {{-- <th>Phone</th> --}}
                                <th>Activity</th>

                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($clientCategories as $key =>  $customer)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $customer->category_name }}</td>
                                    <td>{{ $customer->description }}</td>
                                    <td>{{ $customer->created_at->format('y-d-m') }}</td>
                                    <td><span class="badge badge-success">active</span></td>

                                    <td>


                                        <div class="btn-group">
                                            <button type="button" class="btn btn-secondary dropdown-toggle waves-effect"
                                                data-toggle="dropdown" aria-expanded="false"> Choose Option <i
                                                    class="mdi mdi-chevron-down"></i> </button>
                                            <div class="dropdown-menu" style="">
                                                <!-- item-->
                                                <a href="{{ route('linkcustomer.customercategories', [$customer->id]) }}"
                                                    class="dropdown-item">View Customer in Category</a>
                                                <a href="#edit-custom-modal-{{ $customer->id }}" class="dropdown-item"
                                                    data-animation="fadein" data-plugin="custommodal"
                                                    data-overlaySpeed="200" data-overlayColor="#36404a">Edit Customer
                                                    Category</a>
                                                <a href="javascript:void(0);" class="dropdown-item delete"
                                                    data-id="{{ $customer->id }}">Delete Client</a>

                                                <form class="postdestroy" id="form_{{ $customer->id }}"
                                                    style="margin: 0;" method="Post"
                                                    action="{{ route('customercategories.destroy', $customer->id) }}"
                                                    data-toggle="modal" data-target="#exampleModal">
                                                    @csrf
                                                    @method('Delete')
                                                </form>

                                                <!-- Modal -->
                                                <div id="edit-custom-modal-{{ $customer->id }}" class="modal-demo">
                                                    <button type="button" class="close"
                                                        onclick="Custombox.modal.close();">
                                                        <span>&times;</span><span class="sr-only">Close</span>
                                                    </button>
                                                    <h4 class="custom-modal-title">Fill Client Item Detail </h4>
                                                    <div class="custom-modal-text text-left">
                                                        <form method="POST"
                                                            action="{{ route('customercategories.update', [$customer->id]) }}"
                                                            enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PATCH')
                                                            {{-- <input type="hidden" name="portfolioid" value="{{$portfolioId}}">
                                                    <input type="hidden" name="feature_type" value="Portfolio Testimonial"> --}}
                                                            <div class="form-group">
                                                                <label for="name"> Category Name</label>
                                                                <input type="text" class="form-control"
                                                                    name="category_name" parsley-trigger="change" required
                                                                    value="{{ $customer->category_name }}"
                                                                    placeholder="Enter customer name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name"> Description </label>
                                                                <input type="text" class="form-control" name="description"
                                                                    parsley-trigger="change"
                                                                    value="{{ $customer->description }}"
                                                                    placeholder="Enter Description">
                                                            </div>
                                                            {{-- <div class="form-group">
                                                        <label for="name"> Service Start From </label>
                                                        <input type="date" class="form-control"  name="join_date" parsley-trigger="change" required value="{{ $customer->userDetail->first()->join_date }}"
                                                        placeholder="Enter Service Start Date"> to
                                                        <input type="date" class="form-control"  name="expire_date" parsley-trigger="change" required value="{{ $customer->userDetail->first()->leave_date }}"
                                                        placeholder="Enter Service Start Date">
                                                    </div> --}}
                                                            {{-- <div class="form-group">
                                                        <label for="name"> Service End Date</label>
                                                        <input type="date" class="form-control"  name="expire_date" parsley-trigger="change" required
                                                        placeholder="Enter Service Start Date">
                                                    </div> --}}
                                                            {{-- <div class="form-group">
                                                        <label for="name"> Address</label>
                                                        <input type="text" class="form-control"  name="address" parsley-trigger="change" required value="{{ $customer->userDetail->first()->permanent_address }}"
                                                        placeholder="Enter Address">
                                                    </div> --}}
                                                            {{-- <div class="form-group">
                                                        <label for="name"> Phone </label>
                                                        <input type="text" class="form-control"  name="phone" parsley-trigger="change" required value="{{ $customer->userDetail->first()->phone }}"
                                                        placeholder="Enter Phone">
                                                    </div> --}}
                                                            <button type="submit"
                                                                class="btn btn-success waves-effect waves-light mr-1">Save</button>
                                                            <button type="button"
                                                                class="btn btn-danger waves-effect waves-light"
                                                                onclick="Custombox.modal.close();">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end col -->


    </div>

    <div class="modal fade gift-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="customerCategoryModel">
        <div class="modal-dialog modal-dialog-centered">
            <div id="custom-modal" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Fill Customer Category Item Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="custom-modal-text text-left ">
                    <form method="POST" action="{{ route('customercategories.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="name"> Category Name</label>
                                <input type="text" class="form-control" name="category_name" parsley-trigger="change"
                                    required placeholder="Enter category name">
                            </div>
                            <div class="form-group">
                                <label for="name"> Description </label>
                                <input type="text" class="form-control" name="description" parsley-trigger="change"
                                    placeholder="Enter  Description">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary waves-effect waves-light mr-1"
                                data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1"
                                id="giftBtn">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script>
        $(document).ready(function() {
            // @if (Session::has('message'))
            //     toastr.success('{{ Session::get('message') }}')
            // @endif

            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                // console.log(id);
                // alert('id of current click ',id );
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this imaginary file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your imaginary file has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Your imaginary file is safe!");
                        }
                    });
            })
            $(document).on('click', '#customerCategoryBtn', function() {
                
                $('#customerCategoryModel').modal('show');
            })
        })
    </script>


@endsection
