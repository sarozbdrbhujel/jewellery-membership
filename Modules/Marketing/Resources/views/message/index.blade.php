@extends('dashboard::newLayouts.master')
@section('title')
    Message List
@stop
@section('style')
    <link href="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ Module::asset('dashboard:summernote/summernote.min.css') }}">
    <style>
        .custombox-content>* {
            max-height: unset !important;

        }

        .custombox-content {
            /* overflow-y: initial; */
            /* display: block !important; */
            /* instead of flex */
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-xl-12">
                <div class="card-box">
                    <h4 class="header-title mt-0 mb-3">Message List
                        <a class="btn btn-purple btn-rounded waves-effect w-md waves-light mb-3 float-right"
                            data-toggle="modal" id="modelBtn"><i class="mdi mdi-plus"></i> Create Message</a>

                    </h4>


                    <div class="table-responsive">
                        <table id="clientDatatable" class="table table-hover mb-0">

                            <thead>
                                <tr>
                                    <th>S.N</th>
                                    <th>Message Type</th>
                                    <th>Message</th>
                                    {{-- <th>Email</th> --}}
                                    {{-- <th></th> --}}
                                    <th>Status</th>
                                    {{-- <th>Phone</th> --}}
                                    <th>Activity</th>

                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($messages as $key =>  $customer)
                                    <tr>
                                        <td>{{ $key + 1 }}</td>
                                        <td>{{ $customer->message_type }}</td>
                                        {{-- <td>


                            </td> --}}
                                        {{-- <td>{{ $customer->email }}</td> --}}
                                        <td>{!! htmlspecialchars_decode($customer->message) !!}</td>
                                        <td><span class="badge badge-success">active</span></td>

                                        <td>


                                            <div class="btn-group">
                                                <button type="button"
                                                    class="btn btn-secondary dropdown-toggle waves-effect"
                                                    data-toggle="dropdown" aria-expanded="false"> Choose Option <i
                                                        class="mdi mdi-chevron-down"></i> </button>
                                                <div class="dropdown-menu" style="">
                                                    <!-- item-->
                                                    {{-- <a href="javascript:void(0);" class="dropdown-item">Send Email</a> --}}
                                                    <!-- item-->
                                                    {{-- <a href="javascript:void(0);" class="dropdown-item">Service Reminder</a> --}}
                                                    <!-- item-->
                                                    {{-- <a href="javascript:void(0);" class="dropdown-item">View Detail</a> --}}
                                                    <a href="#edit-custom-modal-{{ $customer->id }}" data-toggle="modal"
                                                        data-id="{{ $customer->id }}" id="editBtn">Edit Message</a>
                                                    <a href="javascript:void(0);" class="dropdown-item delete"
                                                        data-id="{{ $customer->id }}">Delete Client</a>

                                                    <form class="postdestroy" id="form_{{ $customer->id }}"
                                                        style="margin: 0;" method="Post"
                                                        action="{{ route('messages.destroy', $customer->id) }}"
                                                        data-toggle="modal" data-target="#exampleModal">
                                                        @csrf
                                                        @method('Delete')
                                                    </form>

                                                    <!-- Modal -->



                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                    <div id="edit-custom-modal-{{ $customer->id }}" class="modal fade membership-modal-center"
                                        tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel" aria-hidden="true"
                                        style="display: none;">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="custom-modal-title">Edit Messsage Item Detail </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <form method="POST" action="{{ route('messages.update', [$customer->id]) }}"
                                                    enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="subject"> Mail Subject:</label>
                                                            {{-- <textarea name="subject" class="form-control sms" parsley-trigger="change" rows="4" required id="message"> </textarea> --}}
                                                            <input type="text" name="subject" class="form-control" parsley-trigger="change" rows="4" required  placeholder="Enter Email Subject" value="{{ $customer->subject }}">
                                                        </div>
                                                    <div class="form-group">
                                                        <label for="name"> Message:</label>
                                                        <textarea name="message" class="form-control sms" parsley-trigger="change" rows="4" required>{{ $customer->message }} </textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit"
                                                        class="btn btn-success waves-effect waves-light mr-1">Save</button>
                                                    <button type="button" class="btn btn-danger waves-effect waves-light"
                                                        onclick="Custombox.modal.close();">Cancel</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                @endforelse
                            </tbody>
                        </table>


                    </div>


                    <!-- Modal -->
                    
                    <div id="create-modal" class="modal fade membership-modal-center" tabindex="-1" role="dialog"
                        aria-labelledby="myCenterModalLabel" aria-hidden="true" style="display: none;">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myCenterModalLabel">Fill Message Item Detail </h4>
                                    <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">×</button>
                                </div>
                                <form method="POST" action="{{ route('messages.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="name"> Choose Message Type</label>
                                            <select class="select2 js-example-basic-multiple" name="message_type"
                                                multiple="" required multiple data-placeholder="Choose ...">
                                                {{-- @forelse ($clientCategories as $payroll)
                                                            <option value="{{ $payroll->id }}"> {{ $payroll->category_name }} </option>
                                                        @empty

                                                        @endforelse --}}
                                                <option>{{ Modules\Marketing\Enums\MarketingMessageEnum::SMSBirthdayWish }}
                                                </option>
                                                <option>
                                                    {{ Modules\Marketing\Enums\MarketingMessageEnum::EmailBirthdayWish }}
                                                </option>
                                                <option>
                                                    {{ Modules\Marketing\Enums\MarketingMessageEnum::SMSAnniversarydayWish }}
                                                </option>
                                                <option>
                                                    {{ Modules\Marketing\Enums\MarketingMessageEnum::EmailAnniversarydayWish }}
                                                </option>
                                                <option>{{ Modules\Marketing\Enums\MarketingMessageEnum::SMSWelcomeWish }}
                                                </option>
                                                <option>
                                                    {{ Modules\Marketing\Enums\MarketingMessageEnum::EmailWelcomeWish }}
                                                </option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="subject"> Mail Subject:</label>
                                            {{-- <textarea name="subject" class="form-control sms" parsley-trigger="change" rows="4" required id="message"> </textarea> --}}
                                            <input type="text" name="subject" class="form-control" parsley-trigger="change" rows="4" required  placeholder="Enter Email Subject">
                                        </div>
                                        <div class="form-group">
                                            <label for="name"> Message:</label>
                                            <textarea name="message" class="form-control sms" parsley-trigger="change" rows="4" required id="message"> </textarea>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!--model close -->
                </div>
            </div>
        </div>
    </div>


@stop

@section('script')
    <script src="{{ Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- Datatables init -->
    {{-- <script src="{{ Module::asset('dashboard:js/pages/datatables.init.js') }}"></script> --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{ Module::asset('dashboard:summernote/summernote.min.js') }}"></script>
    <script>
        
        $(document).ready(function() {
            $('.sms').summernote({
                placeholder: "Write email description.....",
                tabsize: 2,
                height: 200,
                toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ],
            });
            $('#clientDatatable').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    // 'copy', 'csv', 'excel', 'pdf', 'print',
                    {
                        extend: 'pdf',
                        // text: 'Download',
                        text: '<i class="fa fa-file"></i> Download',
                        className: 'btn btn-purple btn-rounded w-md waves-effect waves-light',
                        key: {
                            key: 'c',
                            altKey: true
                        },
                        exportOptions: {
                            columns: [1, 2, 3]
                        }

                    },


                ]
            });


            //     new Custombox.modal({
            // // Options
            // overlay : {
            //     active: true,
            //     color: '#000',
            //     opacity: .48,
            //     close: true,
            //     speedIn: 300,
            //     speedOut: 300,
            //     onOpen: null,
            //     onComplete: null,
            //     onClose: null,
            // }
            // });

            @if (Session::has('message'))
                //  toastr.info("Have fun storming the castle!")
                toastr.success('{{ Session::get('message') }}')
            @endif

            $('.js-example-basic-multiple').select2({
                dropdownParent: $('#create-modal')
            });

            $('.js-example-basic-multiple-edit').select2({
                dropdownParent: $('.modal-demo')
            });

            $(document).on('click', '#modelBtn', function() {
                $('#create-modal').modal('show');
            })
            $(document).on('click', '#editBtn', function() {
                var customerId = $(this).data('id');
                $('#edit-custom-modal-customerId').modal('show');
            })
            // $(".custombox-modal-wrapper").animate({
            //     scrollTop: $("#custom-modal").offset().top
            // }, 800);

            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                // console.log(id);
                // alert('id of current click ',id );
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this imaginary file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your imaginary file has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Your imaginary file is safe!");
                        }
                    });
            })

        })
    </script>


@endsection
