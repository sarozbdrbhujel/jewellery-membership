
            <li class="menu-title">Marketing and Business</li>
            <li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-bullhorn"></i>
                        <span> @lang('language.'. Modules\AccessManagement\Enums\ModulesEnum::Marketing ) </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="{{ route('customercategories.index') }}">Customer Category</a></li>
                    <li><a href="{{ route('customers.index') }}">Customer</a></li>
                    <li><a href="{{ route('marketing.officer') }}">Marketing Officer</a></li>
                    <li><a href="{{ route('customers.email') }}">Compose Mail</a></li>
                    </ul>
                </li>
            </li>

            <li class="menu-title">Messages And Wish</li>
            <li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="fas fa-bullhorn"></i>
                        <span> @lang('language.'. Modules\AccessManagement\Enums\ModulesEnum::Message ) </span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                    <li><a href="{{ route('messages.index') }}">Wish Messages</a></li>
                    {{-- <li><a href="{{ route('customers.index') }}">Customer</a></li>
                    <li><a href="{{ route('marketing.officer') }}">Marketing Officer</a></li>
                    <li><a href="{{ route('customers.email') }}">Compose Mail</a></li> --}}
                    </ul>
                </li>
            </li>
