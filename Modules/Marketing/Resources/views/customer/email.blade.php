@extends('dashboard::newLayouts.master')
@section('title')
    Compose Email
@endsection
@section('style')

{{-- <link href="{{ Module::asset('dashboard:libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
<link href="{{ Module::asset('dashboard:libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
{{-- <link href="{{ Module::asset('dashboard:libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" />  --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" charset="utf-8" href="{{ Module::asset('dashboard:sn2/summernote.min.css') }}">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
type="text/css" />
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="col-md-12">
        <form action="{{ route('customer.bulkemail') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <div class="form-group col-sm-7">

                <div class="form-group">
                    
                    <div class="radio radio-pink float-right">
                        <input type="radio" name="sendtype" id="radio1" value="email"  parsley-trigger="change" required>
                        <label for="radio1">
                            Email Only
                        </label>
                        <input type="radio" name="sendtype" id="radio2" value="sms" parsley-trigger="change" required checked>
                        <label for="radio2">
                            SMS Only
                        </label>
                        <input type="radio" name="sendtype" id="radio3" value="both" parsley-trigger="change" required >
                        <label for="radio3">
                            Both
                        </label>
                    </div>
                    <button class="btn btn-info">
                        Send Email
                    </button>


                </div>
            </div>
            <div class="form-group col-sm-7">
                <div class="form-group">

                    <div class="checkbox checkbox-purple">
                        <input id="checkbox6a" type="checkbox" class="sendlater">
                        <label for="checkbox6a">
                            Send It Later
                        </label>
                    </div>
                    <input type="datetime-local" class="form-control" value="" name=" time" id="time" >
                </div>
            </div>
            <div class="form-group col-sm-7">
                <label for="name"> Choose Group</label>
                <select class="js-example-basic-multiple" name="client_category_id[]" multiple="multiple" multiple data-placeholder="Choose ..." parsley-trigger="change" required>
                        @forelse ($clientCategory as $category)
                <option value="{{ $category->id }}"> {{ $category->category_name }} </option>
                        @empty

                        @endforelse
                </select>
            </div>


            <div class="form-group col-sm-7">
                <label for="name"> Choose Send From Address</label>
                <select class="js-example-basic-multiple2" name="marketingofficer"  multiple data-placeholder="Choose ..." parsley-trigger="change" required>
                        @forelse ($marketingOfficers as $payroll)
                <option value="{{ $payroll->email_address }}"> {{ $payroll->name }} </option>
                        @empty

                        @endforelse
                </select>
            </div>

                <div class="form-group col-sm-7">
                    <div class="form-group">
                        <label for="name"> Subject </label>
                        <input type="text" class="form-control"  name="subject" parsley-trigger="change" required
                        placeholder="Enter subject for email">
                        <input type="checkbox" name="send_copy" id="radio3"  parsley-trigger="change">
                        <label for="radio4">
                            Send me a copy
                        </label>
                    </div>
                </div>
                <div class="form-group col-sm-12" id="emailEditor">
                    <label for="name"> Email Templete</label>
                    <textarea name="tm" id="tiny" class="form-control editor" parsley-trigger="change" required rows="6"> </textarea>
                </div>


                <div class="form-group col-sm-7" id="smsEditor">
                    <label for="name"> SMS (<span id="characterWrite">0 </span>/150)</label>
                    <textarea id="sms_text" name="sms_text" class="form-control sms editor" parsley-trigger="change" rows="4" required> </textarea>
                </div>

            </form>
        </div>
    </div>
    </div>
@stop
@section('script')
@include('marketing::customer.script')
<script charset="utf-8" src="{{ Module::asset('dashboard:sn2/summernote.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script>
    $(document).ready(function() {
            $('.editor').summernote({
                placeholder: "Write email description.....",
                tabsize: 2,
                height: 150
            });
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
        });
</script>
@stop
