<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function(){

        $('.js-example-basic-multiple').select2({
          })
        $('.js-example-basic-multiple2').select2({
          })

    })
  </script>
  <script>
   var route_prefix = "{{ url()->to(config('lfm.url_prefix', config('lfm.prefix'))) }}";
  </script>
{{-- <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}
{{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script> --}}
<script>
    // hi_IN
  var editor_config = {
    path_absolute : "/",
    relative_urls : false,

    remove_script_host : false,
    convert_urls : true,
    selector: ".editor",
    // inline: true,
    // language: "hi_IN",
    // toolbar: 'fontselect',
    font_formats: 'Arial=arial,helvetica,sans-serif;Courier New=courier new,courier,monospace;AkrutiKndPadmini=Akpdmi-n',
    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
    content_style:"@import url('https://fonts.googleapis.com/css2?family=Yantramanav:wght@300&display=swap');",

    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "fontselect insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  // tinymce.init(editor_config);
</script>


<script>

    $(document).ready(function(){
        $('#time').hide();
         @if(Session::has('message'))
        //  toastr.info("Have fun storming the castle!")
         toastr.success('{{ Session::get('message') }}')
         @endif

         $(document).on('click', '.sendlater', function(){
                console.log('toggle click');
                $('#time').toggle();
         })
         $(document).on('keyup', '.sms', function(){
            el = $(this);
            // if(el.val().length >= 150){
                el.val()
                numberOfMessage =  el.val().length;

                // el.val( el.val().substr(0, 150) );
            // } else {
                // $("#characterWrite").text(150-el.val().length);
                // $("#characterWrite").text(Math.trunc(numberOfMessage)+'/'+ el.val().length);
                $("#characterWrite").text(numberOfMessage);
            // }
         })

         if($('#radio1').is(':checked'))
         {
            console.log('email only')
            $('#smsEditor').hide();
            $('#sms_text').prop("required", false);
            $('.editor').prop("required", true);
         }
         $(document).on('change', 'input[type=radio][name="sendtype"]', function(){
            console.log($(this).val());
            if($(this).val() == 'email')
            {
                $('#smsEditor').hide();
                $('#emailEditor').show();
                $('#sms_text').prop("required", false);
                $('.editor').prop("required", true);
            }
            if($(this).val() == 'sms')
            {
                $('#smsEditor').show();
                $('#emailEditor').hide();
                $('#sms_text').prop("required", true);
                $('.editor').prop("required", false);
            }
            if($(this).val() == 'both')
            {
                $('#smsEditor').show();
                $('#emailEditor').show();
                $('#sms_text').prop("required", true);
                $('.editor').prop("required", true);
            }
         })

    })
</script>


  <!-- TinyMCE init -->
  {{-- <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script>
    var editor_config = {
      path_absolute : "",
      selector: "textarea[name=tm]",
      plugins: [
        "link image"
      ],
      relative_urls: false,
      height: 129,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + route_prefix + '?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script> --}}


  {{-- <script>
    $('textarea#tiny').tinymce({
      height: 500,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
      ],
      toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
    });
  </script> --}}