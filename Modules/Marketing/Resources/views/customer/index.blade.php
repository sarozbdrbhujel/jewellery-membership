@extends('dashboard::newLayouts.master')
@section('title')
    Customers List
@stop
@section('style')
{{-- <link href="{{ Module::asset('dashboard:libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
<link href="{{ Module::asset('dashboard:libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

<style>
    /* .custombox-open,
.custombox-open body {
overflow: hidden;
} */
.custombox-content>* {
  max-height: unset !important;
  /* instead of 95% */
}
.custombox-content {
    /* overflow-y: initial; */
  /* display: block !important; */
  /* instead of flex */
}
/* .custom-modal {
    overflow: scroll !important;
    max-height: unset !important;
} */
    </style>
@endsection
@section('content')

<div class="row">
    <div class="col-xl-12">
        <div class="card-box">
            <div class="dropdown float-right">
                <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false">
                    <i class="mdi mdi-dots-vertical"></i>
                </a> &nbsp;
            </div>

            <h4 class="header-title mt-0 mb-3">Client List
                <a href="#custom-modal" class="btn btn-purple btn-rounded w-md waves-effect waves-light mb-3 float-right"  data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="mdi mdi-plus"></i> Create Client</a>
            </h4>


            <div class="table-responsive">
                <table id="clientDatatable" class="table table-hover mb-0">
                    {{-- <a href="#custom-modal" class="btn btn-purple btn-rounded w-md waves-effect waves-light mb-3 float-left"  data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a"><i class="mdi mdi-plus"></i> Create Client</a> --}}
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Client Name</th>
                        <th>Category (Group user)</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Status</th>
                        {{-- <th>Phone</th> --}}
                        <th>Activity</th>

                    </tr>
                    </thead>
                    <tbody>
                        @forelse ($customers as $key =>  $customer)

                        <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $customer->name }}</td>
                        <td>
                            @forelse ($customer->clientCategories as $user)
                            <span class="badge badge-info">{{ $user->category_name }} &nbsp; </span>
                            @empty

                            @endforelse

                        </td>
                        <td>{{ $customer->email }}</td>
                        <td>{{ $customer->userDetail->first()->phone }}</td>
                            <td><span class="badge badge-success">active</span></td>

                            <td>


                                  <div class="btn-group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle waves-effect" data-toggle="dropdown" aria-expanded="false"> Choose Option <i class="mdi mdi-chevron-down"></i> </button>
                                    <div class="dropdown-menu" style="">
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Send Email</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">Service Reminder</a>
                                        <!-- item-->
                                        <a href="javascript:void(0);" class="dropdown-item">View Detail</a>
                                        <a href="#edit-custom-modal-{{ $customer->id }}" class="dropdown-item"  data-animation="fadein" data-plugin="custommodal" data-overlaySpeed="200" data-overlayColor="#36404a">Edit Client</a>
                                    <a href="javascript:void(0);" class="dropdown-item delete" data-id="{{ $customer->id }}" >Delete Client</a>

                                        <form class="postdestroy" id="form_{{$customer->id}}" style="margin: 0;" method="Post" action="{{ route('customers.destroy', $customer->id) }}"  data-toggle="modal" data-target="#exampleModal" >
                                            @csrf
                                            @method('Delete')
                                        </form>

                                         <!-- Modal -->
                                        <div id="edit-custom-modal-{{ $customer->id }}" class="modal-demo">
                                            <button type="button" class="close" onclick="Custombox.modal.close();">
                                                <span>&times;</span><span class="sr-only">Close</span>
                                            </button>
                                            <h4 class="custom-modal-title">Fill Client Item Detail </h4>
                                            <div class="custom-modal-text text-left">
                                                <form method="POST" action="{{ route('customers.update', [$customer->id]) }}" enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PATCH')
                                                    <div class="form-group">
                                                        <label for="name"> Client Name</label>
                                                    <input type="text" class="form-control"  name="name" parsley-trigger="change" required value="{{ $customer->name }}"
                                                        placeholder="Enter Client name">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name"> Email </label>
                                                        <input type="text" class="form-control"  name="email" parsley-trigger="change" required value="{{ $customer->email }}"
                                                        placeholder="Enter Client Email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name"> Date Of Birth </label>
                                                        <input type="date" class="form-control"  name="dob" parsley-trigger="change" required value="@if(!is_null( $customer->userDetail->first()->dob)){{ $customer->userDetail->first()->dob->format('m/d/Y')  }} @endif"
                                                        placeholder="Enter Client DOB">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name"> Marriage Anniversary (optional)</label>
                                                        {{-- @if(!is_null( $customer->userDetail->first()->merriage_anniversary)) {{ $customer->userDetail->first()->merriage_anniversary->format('m/d/Y') }} @endif --}}
                                                        <input type="date" class="form-control"  name="anniversary" parsley-trigger="change"  value="@if(!is_null( $customer->userDetail->first()->merriage_anniversary)){{ $customer->userDetail->first()->merriage_anniversary->format('m/d/Y') }} @endif"
                                                        placeholder="Enter Client Anniversary">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="name"> Choose Customers Category</label>
                                                        <select class="select2 select2-multiple js-example-basic-multiple-edit" name="client_category_id[]" multiple="multiple" required multiple data-placeholder="Choose ...">
                                                                @forelse ($clientCategories as $payroll)
                                                                    @if($customer->clientCategories->isNotEmpty())
                                                                        @forelse ($customer->clientCategories as $category)
                                                                            @if($payroll->id == $category->id)
                                                                            <option value="{{ $payroll->id }}" selected> {{ $payroll->category_name }} </option>
                                                                            @else
                                                                            <option value="{{ $payroll->id }}"> {{ $payroll->category_name }} </option>
                                                                            @endif
                                                                        @empty

                                                                        @endforelse
                                                                    @else
                                                                    <option value="{{ $payroll->id }}"> {{ $payroll->category_name }} </option>

                                                                    @endif
                                                                    @empty

                                                                @endforelse
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name"> Address</label>
                                                        <input type="text" class="form-control"  name="address" parsley-trigger="change" required value="{{ $customer->userDetail->first()->permanent_address }}"
                                                        placeholder="Enter Address">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="name"> Phone </label>
                                                        <input type="text" class="form-control"  name="phone" parsley-trigger="change" required value="{{ $customer->userDetail->first()->phone }}"
                                                        placeholder="Enter Phone">
                                                    </div>
                                                    <button type="submit" class="btn btn-success waves-effect waves-light mr-1">Save</button>
                                                    <button type="button" class="btn btn-danger waves-effect waves-light" onclick="Custombox.modal.close();">Cancel</button>
                                                </form>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                            </td>
                        </tr>
                        @empty

                        @endforelse
                    </tbody>
                </table>


            </div>


            <!-- Modal -->
            <div id="custom-modal" class="modal-demo">
                <button type="button" class="close" onclick="Custombox.modal.close();">
                    <span>&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="custom-modal-title">Fill Client Item Detail </h4>
                <div class="custom-modal-text text-left ">
                    <form method="POST" action="{{ route('customers.store') }}" enctype="multipart/form-data">
                        @csrf
                        {{-- <input type="hidden" name="portfolioid" value="{{$portfolioId}}">
                        <input type="hidden" name="feature_type" value="Portfolio Testimonial"> --}}
                        <div class="form-group">
                            <label for="name"> Client Name</label>
                            <input type="text" class="form-control"  name="name" parsley-trigger="change" required
                            placeholder="Enter client name">
                        </div>
                        <div class="form-group">
                            <label for="name"> Email </label>
                            <input type="text" class="form-control"  name="email" parsley-trigger="change" required
                            placeholder="Enter client email">
                        </div>
                        <div class="form-group">
                            <label for="name"> Date Of Birth </label>
                            <input type="date" class="form-control"  name="dob" parsley-trigger="change" required value=""
                            placeholder="Enter Client DOB">
                        </div>
                        <div class="form-group">
                            <label for="name"> Marriage Anniversary (optional)</label>
                            <input type="date" class="form-control"  name="anniversary" parsley-trigger="change"  value=""
                            placeholder="Enter Client DOB">
                        </div>

                        {{-- <div class="form-group">
                            <label for="name"> Service Start From </label>
                            <input type="date" class="form-control"  name="join_date" parsley-trigger="change" required
                            placeholder="Enter Service Start Date"> to
                            <input type="date" class="form-control"  name="expire_date" parsley-trigger="change" required
                            placeholder="Enter Service Start Date">
                        </div> --}}
                        {{-- <div class="form-group">
                            <label for="name"> Service End Date</label>
                            <input type="date" class="form-control"  name="expire_date" parsley-trigger="change" required
                            placeholder="Enter Service Start Date">
                        </div> --}}
                        <div class="form-group">
                            <label for="name"> Choose Customers Category</label>
                            <select class="select2 select2-multiple js-example-basic-multiple" name="client_category_id[]" multiple="multiple" required multiple data-placeholder="Choose ...">
                                    @forelse ($clientCategories as $payroll)
                            <option value="{{ $payroll->id }}"> {{ $payroll->category_name }} </option>
                                    @empty

                                    @endforelse
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="name"> Address</label>
                            <input type="text" class="form-control"  name="address" parsley-trigger="change" required
                            placeholder="Enter Address">
                        </div>
                        <div class="form-group">
                            <label for="name"> Phone </label>
                            <input type="text" class="form-control"  name="phone" parsley-trigger="change" required
                            placeholder="Enter Phone">
                        </div>
                        <button type="submit" class="btn btn-success waves-effect waves-light mr-1">Save</button>
                        <button type="button" class="btn btn-danger waves-effect waves-light" onclick="Custombox.modal.close();">Cancel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('script')
        <!-- Datatables init -->
        {{-- <script src="{{ Module::asset('dashboard:js/pages/datatables.init.js') }}"></script> --}}
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

     <script>

       $(document).ready(function(){

        $('#clientDatatable').DataTable( {
            dom: 'Bfrtip',
            buttons: [
                // 'copy', 'csv', 'excel', 'pdf', 'print',
            {
                extend: 'pdf',
                // text: 'Download',
                text: '<i class="fa fa-file"></i> Download',
                className : 'btn btn-purple btn-rounded w-md waves-effect waves-light',
                key: {
                    key: 'c',
                    altKey: true
                },
                exportOptions: {
                    columns: [1,2,3,4,5]
                }

            },


            ]
        } );


    //     new Custombox.modal({
    // // Options
    // overlay : {
    //     active: true,
    //     color: '#000',
    //     opacity: .48,
    //     close: true,
    //     speedIn: 300,
    //     speedOut: 300,
    //     onOpen: null,
    //     onComplete: null,
    //     onClose: null,
    // }
    // });

            @if(Session::has('message'))
           //  toastr.info("Have fun storming the castle!")
            toastr.success('{{ Session::get('message') }}')
            @endif

            $('.js-example-basic-multiple').select2({
            dropdownParent: $('#custom-modal')
            });

            $('.js-example-basic-multiple-edit').select2({
            dropdownParent: $('.modal-demo')
            });


            $(".custombox-modal-wrapper").animate({
                scrollTop: $("#custom-modal").offset().top
            }, 800);

            $(document).on('click', '.delete', function(){
                id = $(this).data('id');
                // console.log(id);
                // alert('id of current click ',id );
                swal({
                    title: "Are you sure?",
                    text: "Once deleted, you will not be able to recover this imaginary file!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                    })
                    .then((willDelete) => {
                    if (willDelete) {
                        $('#form_'+id).submit();
                        swal("Poof! Your imaginary file has been deleted!", {
                        icon: "success",

                        });
                    } else {
                        swal("Your imaginary file is safe!");
                    }
                    });
            })

        })
    </script>


@endsection
