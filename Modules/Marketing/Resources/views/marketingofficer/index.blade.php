@extends('dashboard::newLayouts.master')
@section('title')
    Merketing Officer
@endsection
@section('content')


    <div class="row">
        <div class="col-xl-11">
            <div class="card-box">
                <h4 class="header-title mt-0 mb-3">Officer List
                    <a class="btn btn-purple btn-rounded w-md waves-effect waves-light mb-3 float-right"data-toggle="modal"
                        id="marketingBtn"> Create Marketing Officer</a>
                </h4>


                <div class="table-responsive">
                    <table class="table table-hover mb-0">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Officer Name</th>
                                <th>Email Address</th>

                                <th>Status</th>
                                {{-- <th>Phone</th> --}}
                                <th>Activity</th>

                            </tr>
                        </thead>
                        <tbody>
                            {{-- @dd($customers) --}}
                            @forelse ($marketingOfficer as $key =>  $customer)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->email_address }}</td>
                                    {{-- <td><span class="badge badge-danger">expired</span></td> --}}
                                    <td><span class="badge badge-success">active</span></td>

                                    <td>


                                        <div class="btn-group">
                                            <button type="button" class="btn btn-secondary dropdown-toggle waves-effect"
                                                data-toggle="dropdown" aria-expanded="false"> Choose Option <i
                                                    class="mdi mdi-chevron-down"></i> </button>
                                            <div class="dropdown-menu" style="">
                                                <!-- item-->
                                                <a href="#edit-custom-modal-{{ $customer->id }}" class="dropdown-item"
                                                    data-animation="fadein" data-plugin="custommodal"
                                                    data-overlaySpeed="200" data-overlayColor="#36404a">Edit Client</a>
                                                <a href="javascript:void(0);" class="dropdown-item delete"
                                                    data-id="{{ $customer->id }}">Delete Client</a>

                                                <form class="postdestroy" id="form_{{ $customer->id }}"
                                                    style="margin: 0;" method="Post"
                                                    action="{{ route('marketing.officer.delete', $customer->id) }}"
                                                    data-toggle="modal" data-target="#exampleModal">
                                                    @csrf
                                                    @method('Delete')
                                                </form>

                                                <!-- Modal -->
                                                <div id="edit-custom-modal-{{ $customer->id }}" class="modal-demo">
                                                    <button type="button" class="close"
                                                        onclick="Custombox.modal.close();">
                                                        <span>&times;</span><span class="sr-only">Close</span>
                                                    </button>
                                                    <h4 class="custom-modal-title">Fill Client Item Detail </h4>
                                                    <div class="custom-modal-text text-left">
                                                        <form method="POST"
                                                            action="{{ route('marketing.officer.update', [$customer->id]) }}"
                                                            enctype="multipart/form-data">
                                                            @csrf
                                                            @method('PATCH')
                                                            {{-- <input type="hidden" name="portfolioid" value="{{$portfolioId}}">
                                                    <input type="hidden" name="feature_type" value="Portfolio Testimonial"> --}}
                                                            <div class="form-group">
                                                                <label for="name"> Officer Email Name</label>
                                                                <input type="text" class="form-control" name="name"
                                                                    parsley-trigger="change" required
                                                                    value="{{ $customer->name }}"
                                                                    placeholder="Enter Officer name">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="name"> Email </label>
                                                                <input type="text" class="form-control"
                                                                    name="email_address" parsley-trigger="change" required
                                                                    value="{{ $customer->email_address }}"
                                                                    placeholder="Enter Officer  Email">
                                                            </div>

                                                            <button type="submit"
                                                                class="btn btn-success waves-effect waves-light mr-1">Save</button>
                                                            <button type="button"
                                                                class="btn btn-danger waves-effect waves-light"
                                                                onclick="Custombox.modal.close();">Cancel</button>
                                                        </form>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>

                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- end col -->
    </div>

    <!-- Modal -->
    <div class="modal fade gift-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="marketingModel">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Fill Officer Detail</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form method="POST" action="{{ route('marketing.officer.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="name"> Officer Email Name</label>
                            <input type="text" class="form-control" name="name" parsley-trigger="change" required
                                placeholder="Enter client name">
                        </div>
                        <div class="form-group">
                            <label for="name"> Officer Email Name </label>
                            <input type="text" class="form-control" name="email_address" parsley-trigger="change" required
                                placeholder="Enter client email">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="SUBMIT" class="btn btn-primary" id="giftBtn">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop

@section('script')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <script>
        $(document).ready(function() {
            @if (Session::has('message'))
                //  toastr.info("Have fun storming the castle!")
                toastr.success('{{ Session::get('message') }}')
            @endif

            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                // console.log(id);
                // alert('id of current click ',id );
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this imaginary file!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your imaginary file has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Your imaginary file is safe!");
                        }
                    });
            })
            $(document).on('click', '#marketingBtn', function() {

                $('#marketingModel').modal('show');
            })
        })
    </script>


@endsection
