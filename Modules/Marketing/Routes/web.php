<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'marketing','middleware'=>['auth']
],function() {
    Route::get('/', 'MarketingController@index');
    // Route::resource('customers', 'CustomerController');

    Route::resource('customercategories', 'CustomerCategoryController');
    Route::get('link/customers/{id}', 'CustomerCategoryController@linkCustomers')->name('linkcustomer.customercategories');
    Route::post('store/link/customers/{id}', 'CustomerCategoryController@postLinkCustomers')->name('post.linkcustomer.customercategories');
    Route::get('email/customers/', 'CustomerController@emailTemplate')->name('customers.email');
    Route::post('/customers/bulk-email', 'CustomerController@sendCustomeBulkEmail')->name('customer.bulkemail');
    Route::get('officers', 'MarketingController@marketingOfficer' )->name('marketing.officer');
    Route::post('officers/store', 'MarketingController@marketingOfficerStore' )->name('marketing.officer.store');
    Route::patch('officers/update/{id}', 'MarketingController@marketingOfficerUpdate' )->name('marketing.officer.update');
    Route::delete('officers/delete/{id}', 'MarketingController@marketingOfficerDelete' )->name('marketing.officer.delete');

    Route::resource('messages', 'MessageController');
});
