<?php

namespace Modules\Marketing\Console;

use App\User;
use Illuminate\Console\Command;
// use Modules\AccessManagement\Entities\Module;
use Modules\Marketing\Customer\Customer;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BirthdayWishCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'birthday:wish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'command for birthday wish of customer.';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Customer $customer)
    {
        $this->customer = $customer ;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $runTask = $this->customer->customerList();
        \Log::info("Cron is working fine!");
        $this->info('execute a birthday wish command from schedule');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    // protected function getArguments()
    // {
    //     return [
    //         ['example', InputArgument::REQUIRED, 'An example argument.'],
    //     ];
    // }

    // /**
    //  * Get the console command options.
    //  *
    //  * @return array
    //  */
    // protected function getOptions()
    // {
    //     return [
    //         ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
    //     ];
    // }
}
