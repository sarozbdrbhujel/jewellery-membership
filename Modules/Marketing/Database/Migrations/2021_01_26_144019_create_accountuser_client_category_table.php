<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateaccountuserClientCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accountuser_client_category', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('accountuser_id');
            // $table->foreign('accountuser_id')
                // ->references('accountusers')
                // ->on('id')
                // ->onDelete('cascade')
                // ->onUpdate('cascade');
            $table->unsignedBigInteger('client_category_id');
            // $table->foreign('client_category_id')
                // ->references('client_categories')
                // ->on('id')
                // ->onDelete('cascade')
                // ->onUpdate('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_categories');
    }
}
