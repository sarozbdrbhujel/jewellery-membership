<?php

namespace Modules\Marketing\Entities;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guarded = [];
}
