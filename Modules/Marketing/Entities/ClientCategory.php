<?php

namespace Modules\Marketing\Entities;

use Illuminate\Database\Eloquent\Model;
// use Modules\Humanresource\Entities\Accountuser;
use Modules\Customer\Entities\Customer;

class ClientCategory extends Model
{
    protected $guarded = [];

    public function accountUsers()
    {
        return $this->belongsToMany(Customer::class, 'accountuser_client_category', 'client_category_id', 'accountuser_id');
    }
}
