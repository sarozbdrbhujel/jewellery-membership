<?php

namespace Modules\Marketing\Entities;

use Illuminate\Database\Eloquent\Model;

class MarketingEmail extends Model
{
    protected $guarded = [];
}
