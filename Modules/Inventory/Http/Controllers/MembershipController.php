<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Http\Requests\MembershipRequest;
use Modules\Inventory\Interfaces\MembershipInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Helpers\MembershipShortNameHelper;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(MembershipInterface $membershipInterface)
    {
        $this->membershipInterface=$membershipInterface;
    }
    public function index()
    {
        $data['membership']=$this->membershipInterface->getAll();
        return view('inventory::membership.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('inventory::membership.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(MembershipRequest $request)
    {
        try{
            DB::beginTransaction();
            
            $membership=$this->membershipInterface->store($request->all());
        }catch(Exception $e){

            DB::rollback();
            
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('membership.index')->with('message', 'Successfully Created New Privilege Card');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::membership.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['membership']=$this->membershipInterface->findOrFail($id);
        return view('inventory::membership.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(MembershipRequest $request, $id)
    {
        try{
            DB::beginTransaction();
            $membership=$this->membershipInterface->update($request->all(),$id);
        }catch(Exception $e){

            DB::rollback();
            
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('membership.index')->with('message', 'Successfully Updated The Privilege Card');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $this->membershipInterface->delete($id);
        }catch(Exception $e){
            return response($e->getMessage());
        }
        return redirect()->route('membership.index')->with('message','Privilege Card Deleted Successfully');
    }
}
