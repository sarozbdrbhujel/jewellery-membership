<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Http\Requests\PurchaseRequest;
use Modules\Inventory\Interfaces\PurchaseInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Inventory\Interfaces\MembershipInterface;
use Modules\Inventory\Interfaces\CardInterface;
use Modules\Inventory\Entities\PurchaseDetails;
use Modules\Marketing\Enums\MarketingMessageEnum;
use Modules\Marketing\Entities\Message;
use Modules\Marketing\Emails\BirthdayMail;
use Mail;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(PurchaseInterface $purchaseInterface,CustomerInterface $customerInterface,MembershipInterface $membershipInterface,CardInterface $cardInterface)
    {
        $this->customerInterface=$customerInterface;
        $this->purchaseInterface=$purchaseInterface;
        $this->membershipInterface=$membershipInterface;
        $this->cardInterface=$cardInterface;
    }
    public function index()
    {
        $data['purchase']=$this->purchaseInterface->with(['customer.card','details'])->get();
        
        return view('inventory::purchase.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['customers']=$this->customerInterface->with(['card'])->get();
        $data['memberships']=$this->membershipInterface->pluck('name','id');
        return view('inventory::purchase.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(PurchaseRequest $request)
    {
        // dd($request->all());
        try{
            DB::beginTransaction() ;
            if(!is_null($request->customer_id)){
                $purchaseData=[
                    'customer_id'=>$request->customer_id,
                    // 'amount_id'=>$request->amount,
                ];
                $purhcase=$this->purchaseInterface->store($purchaseData);
                $totAmt=0;
                foreach($request->amount as $key=>$value){
                    // dd($key,$value);
                    $totAmt=$totAmt+$request->amount[$key];
                    $purchaseDetailsData=[
                        'purchase_id'=>$purhcase->id,
                        'amount'=>$request->amount[$key],
                        'bill_nos'=>$request->bill_no[$key],
                        // 'amount_id'=>$request->amount,
                    ];
                    $purchaseDetails=PurchaseDetails::Create($purchaseDetailsData);
                }
                $customer=$this->customerInterface->with(['purchases','card.membership'])->findOrFail($request->customer_id);
                $totaluCustomerAmount=$totAmt;
                if(!is_null($customer->card)){
                    $card= $this->cardInterface->findOrFail($customer->card->id);
                    $rewards_point=($totaluCustomerAmount/$customer->card->membership->amount_to_point)+$card->rewards_point;
                    
                    // dd($rewards_point,$card);
                    $this->cardInterface->update(['rewards_point'=>$rewards_point],$customer->card->id);
                    
                }
                if(is_null($customer->card)){
                    
                    $memberships=$this->membershipInterface->getAll();
                    foreach($memberships as $membership){
                        if(($membership->min_amt <= $totaluCustomerAmount) 
                        &&
                         ($membership->max_amt >=$totaluCustomerAmount))
                         {
                            $membershipFound=$membership->name;
                        }else{
                            $membershipFound=null;
                        }
                    }
    
                }
            }else{
                $customerData=[
                    'name'=>$request->name,
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    'address'=>$request->address,
                    'aniversary_date'=>$request->aniversary_date,
                    'spouse_name'=>$request->spouse_name,
                    'mobile'=>$request->mobile,
                    'birth_date'=>$request->birth_date,
                    'designation'=>$request->designation,
                ];
                $customer=$this->customerInterface->store($customerData);
                if($customer && !is_null($customer->email)){

                    $welcomemail=Message::where('message_type',MarketingMessageEnum::EmailWelcomeWish)->first();
                    $custmoEmail = new BirthdayMail($welcomemail->subject,$welcomemail->message,$customer->name);
                    // dd($custmoEmail,$cus->welcomemail);
                    Mail::to($customer->email)->send($custmoEmail);
                }
                $cardData=[
                    'membership_id'=>$request->membership_id,
                    'customer_id'=>$customer->id,
                ];
                $card=$this->cardInterface->store($cardData);
                // dd(   $card,$customer, $cardData);
                
                $purchaseData=[
                    'customer_id'=>$customer->id,
                    // 'amount_id'=>$request->amount,
                ];
                $purhcase=$this->purchaseInterface->store($purchaseData);
                $totAmt=0;
                foreach($request->amount as $key=>$value){
                    // dd($key,$value);
                    $totAmt=$totAmt+$request->amount[$key];
                    $purchaseDetailsData=[
                        'purchase_id'=>$purhcase->id,
                        'amount'=>$request->amount[$key],
                        'bill_nos'=>$request->bill_no[$key],
                        // 'amount_id'=>$request->amount,
                    ];
                    // dump($purchaseDetailsData);
                    $purchaseDetails=PurchaseDetails::Create($purchaseDetailsData);
                }
                // dd($totAmt);
                // $customer=$this->customerInterface->with(['purchases','card.membership'])->findOrFail($request->customer_id);
                $totaluCustomerAmount=$totAmt;
                if(!is_null($customer->card)){
                    $card= $this->cardInterface->findOrFail($customer->card->id);
                    $rewards_point=($totAmt/$customer->card->membership->amount_to_point)+$card->rewards_point;
                    
                    // dd($rewards_point,$card);
                    $this->cardInterface->update(['rewards_point'=>$rewards_point],$customer->card->id);
                    
                }
                if(is_null($customer->card)){
                    
                    $memberships=$this->membershipInterface->getAll();
                    foreach($memberships as $membership){
                        if(($membership->min_amt <= $totaluCustomerAmount) 
                        &&
                         ($membership->max_amt >=$totaluCustomerAmount))
                         {
                            $membershipFound=$membership->name;
                        }else{
                            $membershipFound=null;
                        }
                    }
    
                }
            }
        } catch(Exception $e){

            DB::rollback();
            
            return response($e->getMessage());
        }
        
        DB::commit();
        if(isset($membershipFound)){
            return redirect()->route('purchase.index')->with('membershipFound','Upgrade the User to New Memebership card '.$membershipFound)->with('message','New purchase Added Successfully');
        }else{

            return redirect()->route('purchase.index')->with('message','New purchase Added Successfully');
        }
       ;
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::purchase.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('inventory::purchase.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function customerAmounts($customer_id){
        try{

            $customer=$this->customerInterface->with(['purchases','card.membership'])->findOrFail($customer_id);
            $totaluCustomerAmount=$customer->purchases->sum('amount');
            
            if(!is_null($customer->card)){
                $customer->card->update([
                    'rewards_point'=>$totaluCustomerAmount/$customer->card->membership->amount_to_point,
                ]);
            }
            dd($totaluCustomerAmount,$customer);
        }catch(Exception $e){
            DB::rollback();
            
            return response($e->getMessage());
        }
    }
}
