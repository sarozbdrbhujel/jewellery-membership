<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Http\Requests\CardRequest;
use Modules\Inventory\Interfaces\CardInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Inventory\Interfaces\MembershipInterface;
use Modules\Customer\Entities\Customer;

class CardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(CardInterface $cardInterface, MembershipInterface $membershipInterface, CustomerInterface $customerInterface)
    {
        $this->cardInterface = $cardInterface;
        $this->membershipInterface = $membershipInterface;
        $this->customerInterface = $customerInterface;
    }
    public function index()
    {
        $data['cards'] = $this->cardInterface->with(['membership', 'user', 'customer.rewards'])->get();
        return view('inventory::card.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {

        $data['memberships'] = $this->membershipInterface->pluck('name', 'id');
        // $data['customers']=$this->customerInterface->doesntHave(['card'])->pluck('name','id');
        $data['customers'] = Customer::doesntHave('card')->pluck('name', 'id');
        // dd($data['customers']);
        return view('inventory::card.create', $data);
    }
    public function createCustomerCard(CardRequest $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $card = $this->cardInterface->store($request->all());
            // dump($card);
            $customer=$this->customerInterface->with(['purchases','card.membership'])->findOrFail($request->customer_id);
            $totaluCustomerAmount=$customer->purchases->sum('amount');
            $rewards_point=$totaluCustomerAmount/$customer->card->membership->amount_to_point;
            $card->rewards_point=$rewards_point;
            $card->save();
            // dd($card);

        } catch (Exception $e) {

            DB::rollback();

            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('customers.index')->with('message', 'Successfully Created New Member Card');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CardRequest $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();

            $card = $this->cardInterface->store($request->all());
            $customer=$this->customerInterface->with(['purchases','card.membership'])->findOrFail($request->customer_id);
            $totaluCustomerAmount=$customer->purchases->sum('amount');
            $rewards_point=$totaluCustomerAmount/$customer->card->membership->amount_to_point;
            $card->rewards_point=$rewards_point;
            $card->save();
        } catch (Exception $e) {

            DB::rollback();

            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('cards.index')->with('message', 'Successfully Created New Member Card');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::card.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['card'] = $this->cardInterface->findOrFail($id);
        return view('inventory::card.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(CardRequest $request, $id)
    {
        try {
            DB::beginTransaction();
            $membership = $this->cardInterface->update($request->all(), $id);
        } catch (Exception $e) {

            DB::rollback();

            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('cards.index')->with('message', 'Successfully Updated The Member Card');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        // dd($id);
        try {
            $this->cardInterface->delete($id);
        } catch (Exception $e) {
            return response($e->getMessage());
        }
        return redirect()->route('customers.index')->with('message', 'Member Card Deleted Successfully');
    }

    public function membershipCheck(Request $request)
    {
        $card = $this->cardInterface->where(['card_no' => $request->card_no])->first();
        $membership = $this->membershipInterface->findOrFail($request->membership_id);
        $customer = $this->customerInterface->with('purchases')->findOrFail($request->customer_id);
        // dd($customer);
        // $total = 0;
        // foreach ($customer->purchases as $purchase) {
        //     $total = $total + $purchase->amount;
        // }
        $totalPurchase= $customer->purchases->sum('amount');
        // DD($totalPurchase,$membership);
        if (!is_null($card)) {
            return response(['error' => 'Card No. Should Be Uique!! Try Again.']);
        }
        if(!(($membership->min_amt <= $totalPurchase) && ($totalPurchase <= $membership->max_amt))){
            return response(['error' => $customer->name.' Doesnot Meet The Citeria For '.$membership->name.'!! Try Another Membership.']);
        }
        return response(['success'=>'success']);
    }
}
