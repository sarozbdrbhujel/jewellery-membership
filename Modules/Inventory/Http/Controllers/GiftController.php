<?php

namespace Modules\Inventory\Http\Controllers;

use Exception;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Inventory\Interfaces\RewardInterface;
use Modules\Inventory\Interfaces\CardInterface;
use Modules\Inventory\Http\Requests\GiftCustomerRequest;
use Illuminate\Support\Facades\DB;

class GiftController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(RewardInterface $rewardInterface, CustomerInterface $customerInterface, CardInterface $cardInterface)
    {
        $this->rewardInterface = $rewardInterface;
        $this->customerInterface = $customerInterface;
        $this->cardInterface = $cardInterface;
    }
    public function index()
    {
        $data['customers'] = $this->customerInterface->with(['rewards'])->whereHas('rewards')->get();
        // dd($data['customers'][0]->rewards[0]->pivot->point_amt);
        return view('inventory::gifts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['customers'] = $this->customerInterface->with(['rewards'])->whereHas('card')->pluck('name', 'id');
        // dd( $data['customers']);
        $data['rewards'] = $this->rewardInterface->pluck('name', 'id');
        return view('inventory::gifts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(GiftCustomerRequest $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $customer = $this->customerInterface->with('card')->findOrFail($request->customer_id);
            $customerRewardPoints = $customer->card->rewards_point;
            $reward = $this->rewardInterface->findOrFail($request->reward_id);
            $giftdata=[
                'reward_id'=>$request->reward_id,
                'point_amt'=>$customerRewardPoints
            ];
            // dd($giftdata, $customer);
            // $card = $this->cardInterface->findOrFail($customer->card->id);
            // dd($card, $reward);
            // if ($card->rewards_point >= $reward->point_worth) {
                //     $card->rewards_point = $card->rewards_point-$reward->point_worth;
                //     $card->save();
                // }
                if (is_null($customerRewardPoints)) {
                    return redirect()->back()->with('message', 'Customer is not eligiable to recieve Gifts');
                } else {
                    // dd('customer less more');
                    $customer->rewards()->attach($request->reward_id,['point_amt'=>$customerRewardPoints]);
                // return response(['error' => $customer->name . 'does not Have enough reward points']);
            }
        } catch (Exception $e) {

            DB::rollback();

            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('gifts.index')->with('message', 'Gift Allocated To Customer Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::gifts.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('inventory::gifts.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }

    public function checkCustomerGift(Request $request)
    {
        // dd($request->all());
        $customer = $this->customerInterface->with(['card', 'rewards'])->findOrFail($request->customer_id);
        $customerRewardPoints = $customer->card->rewards_point - $customer->rewards->sum('point_worth');
        // dd($customerRewardPoints);
        $reward = $this->rewardInterface->findOrFail($request->reward_id);
        // dd($customer->card->rewards_point,$reward->point_worth);
        if ($customerRewardPoints < $reward->point_worth) {
            // dd('customer less more');
            return response(['error' => $customer->name . 'does not Have enough reward points']);
        } else {
            return response(['success' => 'success']);
        }

        // $rewards=$this->rewardInterface->pluck('name','id');
    }
}
