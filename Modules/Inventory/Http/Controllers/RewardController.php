<?php

namespace Modules\Inventory\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Inventory\Http\Requests\RewardRequest;
use Modules\Inventory\Interfaces\RewardInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class RewardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(RewardInterface $rewardInterface)
    {
        $this->rewardInterface=$rewardInterface;
    }
    public function index()
    {
        $data['rewards']=$this->rewardInterface->getAll();
        return view('inventory::rewards.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('inventory::rewards.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(RewardRequest $request)
    {
        // dd($request->all());
        try{
            DB::beginTransaction();
            $rewards=$this->rewardInterface->store($request->all());
        }catch(Exception $e){

            DB::rollback();
            
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('rewards.index')->with('message', 'Successfully Created New Reward Gift');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('inventory::rewards.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['reward']=$this->rewardInterface->findOrFail($id);
        return view('inventory::rewards.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(RewardRequest $request, $id)
    {
        try{
            DB::beginTransaction();
            $rewards=$this->rewardInterface->update($request->all(),$id);
        }catch(Exception $e){

            DB::rollback();
            
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('rewards.index')->with('message', 'Successfully Updated The Reward Gift');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try{
            $this->rewardInterface->delete($id);
        }catch(Exception $e){
            return response($e->getMessage());
        }
        return redirect()->route('rewards.index')->with('message','Reward Gift Deleted Successfully');
    }
}
