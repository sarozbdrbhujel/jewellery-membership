<?php

namespace Modules\Inventory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MembershipRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        return [
            'name'=>'required',
            'min_amt'=>'required|numeric',
            'max_amt'=>'required|numeric',
            'amount_to_point'=>'required|numeric',
            'short_name'=>'sometimes|unique:memberships,short_name,'.$this->membership,
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
