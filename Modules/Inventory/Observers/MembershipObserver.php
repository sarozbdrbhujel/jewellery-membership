<?php
namespace Modules\Inventory\Observers;
use Modules\Inventory\Helpers\MembershipShortNameHelper;
use Modules\Inventory\Entities\Membership;

class MembershipObserver{
    public function creating(Membership $membership){
        $membership->short_name = !is_null( $membership->short_name)? $membership->short_name:$mem =MembershipShortNameHelper::uniquemembershipShortName($membership->name);
    }
}