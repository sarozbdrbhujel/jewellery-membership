<?php
namespace Modules\Inventory\Observers;
use Modules\Inventory\Helpers\CardShortNameHelper;
use Modules\Inventory\Entities\Card;

class CardObserver{
    public function creating(Card $card){
        // $card->card_no = !is_null( $card->card_no)? $card->card_no:$mem =CardShortNameHelper::uniqucardShortName( $card->membership_id);

        $card->card_no = !is_null( $card->card_no)?$card->card_no:CardShortNameHelper::uniqucardShortName( $card->membership_id,$card->customer_id);
        $card->authorized_by = auth()->user()->id;
        // $mem
    }
}