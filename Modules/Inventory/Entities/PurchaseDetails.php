<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;

class PurchaseDetails extends Model
{
   

    protected $guarded = [];
    
    public function purchase(){
        return $this->belongsTo(Purchase::class,'purchase_id');
    }
}
