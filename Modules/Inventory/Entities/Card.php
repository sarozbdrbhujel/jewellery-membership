<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\Membership;
use App\User;
use Modules\Customer\Entities\Customer;
class Card extends Model
{

    protected $fillable = ['name','status','card_no','customer_id','authorized_by','membership_id','other_informations','rewards_point'];

    public function membership(){
        return $this->belongsTo(Membership::class,'membership_id');
    }
    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function user(){
        return $this->belongsTo(User::class,'authorized_by');
    }
}
