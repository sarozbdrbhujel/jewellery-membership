<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\Customer;
class Reward extends Model
{

    protected $fillable = ['name','point_worth','other_informations','status'];
    public function customers(){
        return $this->belongsToMany(Customer::class,'customer_reward','reward_id','customer_id');
    }
}
