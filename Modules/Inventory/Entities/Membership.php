<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Inventory\Entities\Card;
class Membership extends Model
{

    protected $fillable = ['name','min_amt','max_amt','status','amount_to_point','short_name','other_information'];
    
    public function cards(){
        return $this->hasMany(Card::class,'membership_id');
    }
    
}
