<?php

namespace Modules\Inventory\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\Customer;
class Purchase extends Model
{
    protected $fillable = ['customer_id','amount','invoice','other_informations'];
       
    public function customer(){
        return $this->belongsTo(Customer::class,'customer_id');
    }
    public function details(){
        return $this->hasMany(PurchaseDetails::class,'purchase_id');
    }
}
