{{-- @dd($customer) --}}
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Reward Name <span class="redAstrick"> *</span>:</label>
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Full Name','data-parsley-trigger'=>'keyup','required']) !!}


            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Point Worth<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-8">
                {!! Form::text('point_worth', null, ['class' => 'form-control', 'id' => 'point_worth', 'placeholder' => 'Point Worth','data-parsley-trigger'=>'keyup','required','data-parsley-type'=>'number']) !!}
            </div>
        </div> 
    </div>
    <div class="form-group mb-0 justify-content-end row col-12">
        <div class="col-sm-9">
            <button type="submit" class="btn btn-info waves-effect waves-light float-right">Submit</button>
        </div>
    </div>
</div>