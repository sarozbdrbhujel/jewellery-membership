@extends('dashboard::newLayouts.master')
@section('title')
     Create Reward
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('rewards.index') }}">Rewards</a></li>
                <li class="breadcrumb-item active">Create Reward</li>
            </ol>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Reward Details</h4>

                {{-- <form class="form-horizontal" role="form">
                    
                </form> --}}
                {{-- {!! Form::open(['route'=>'customers.store','method'=>'post','class'=>'needs-validation form-horizontal']) !!} --}}
                {!! Form::open(['route' => 'rewards.store', 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate', 'id' => 'formValidation']) !!}
                {{-- @csrf --}}
                @include('inventory::rewards.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
@endsection
