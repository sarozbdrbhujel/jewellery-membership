@extends('dashboard::newLayouts.master')
@section('title')
    Edit Privilege Card - {{ $membership->name }}
@endsection
@section('content')
    {{-- @dd($customer) --}}
    <div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('membership.index') }}">Privilege Card</a></li>
                <li class="breadcrumb-item active">{{ $membership->name }}</li>
            </ol>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Edit Privilege Card</h4>
                {!! Form::model($membership, ['route' => ['membership.update', $membership->id], 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate', 'id' => 'formValidation']) !!}
                @method('PATCH')
                @include('inventory::membership.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
@endsection
