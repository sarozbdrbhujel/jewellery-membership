{{-- @dd($customer) --}}
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-5 col-form-label">Privilege Card Name <span class="redAstrick"> *</span>:</label>
            <div class="col-sm-7">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Full Name','data-parsley-trigger'=>'keyup','required']) !!}


            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-5 col-form-label">Min Amount<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-7">
                {!! Form::text('min_amt', null, ['class' => 'form-control', 'id' => 'min_amt', 'placeholder' => 'Min Amount','data-parsley-trigger'=>'keyup','required','data-parsley-type'=>'number']) !!}
            </div>
        </div>
        
        
    </div>
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-6 col-form-label">Short Name<span class="redAstrick"><small><i>(can be auto generated)</i></small></span>:</label>
            <div class="col-sm-6">
                {!! Form::text('short_name', null, ['class' => 'form-control', 'id' => 'short_name', 'placeholder' => 'Short Name','data-parsley-trigger'=>'keyup']) !!}
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-5 col-form-label">Max Amount<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-7">
                {!! Form::text('max_amt', null, ['class' => 'form-control', 'id' => 'max_amt', 'placeholder' => 'Max Amount','data-parsley-trigger'=>'keyup','required','data-parsley-type'=>'number']) !!}
            </div>
        </div>
        
        
        
    </div>
    <div class="form-group row col-12">
        <label for="inputEmail3" class="col-sm-6 col-form-label">Amount to Reward Conversion <small class="redAstrick"><i>(how much amount is equal to 1 Reward Point???)</i></small><span class="redAstrick"> *</span>:</label>
        <div class="col-sm-6">
            {!! Form::text('amount_to_point', null, ['class' => 'form-control', 'id' => 'amount_to_point', 'placeholder' => 'Amount To Reward Point Conversion','data-parsley-trigger'=>'keyup','required','data-parsley-type'=>'number' ]) !!}
        </div>
    </div>
    <div class="form-group mb-0 justify-content-end row col-12">
        <div class="col-sm-9">
            <button type="submit" class="btn btn-info waves-effect waves-light float-right">Submit</button>
        </div>
    </div>
</div>