@extends('dashboard::newLayouts.master')
@section('title')
    Privilege Cards
@endsection
@section('style')
    <!-- third party css -->
    <link href="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/select.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <!-- third party css end -->
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item active">Privilege Card</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box table-responsive">
                <h4 class="mt-0 header-title">Privilege Cards</h4>
                {{-- <p class="text-muted font-14 mb-3">
                    Responsive is an extension for DataTables that resolves that problem by optimising the table's layout for different screen sizes through the dynamic insertion and removal of columns from the table.
                </p> --}}

                <table id="responsive-customers" class="table table-bordered table-bordered dt-responsive nowrap">
                    {{-- @can('create', [Modules\Inventory\Entities\Customer::class, Modules\AccessManagement\Enums\ModulesEnum::Customer]) --}}
                    <a class="btn btn-sm btn-info float-right" href="{{ route('membership.create') }}"> <i
                            class="fa fa-plus"> </i> </a>
                    {{-- @endcan --}}

                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Short Name</th>
                            <th>Min. Amount</th>
                            <th>Max. Amount</th>
                            <th>Amount To Reward Point</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($membership as $membership)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $membership->name }}</td>
                                <td>{{ $membership->short_name }}</td>
                                <td>Rs. {{ $membership->min_amt }}</td>
                                <td>Rs. {{ $membership->max_amt }}</td>
                                <td>Rs. {{ $membership->amount_to_point }}</td>
                                <td>
                                    <a class="btn btn-info btn-sm " href="{{route('membership.edit',$membership->id)}}"
                                        >
                                        <i class="fa fa-edit">
                                        </i>
                                    </a>
                                    <a href="javascript:void(0);" class="btn btn-sm btn-danger delete"
                                                data-id="{{ $membership->id }}"><i class="fas fa-trash"></i></a>
                                            &nbsp; &nbsp;
                                            <form class="postdestroy" id="form_{{ $membership->id }}" style="margin: 0;"
                                                method="Post" action="{{ route('membership.destroy', $membership->id) }}"
                                                data-toggle="modal" data-target="#exampleModal">
                                                @csrf
                                                @method('Delete')
                                            </form>



                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    </div> <!-- end row -->
@endsection
@section('script')
    <!-- third party js -->
    <script src="{{ Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- third party js ends -->
    <script>
        $(document).ready(function() {

            $('#responsive-customers').DataTable();
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this data!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your record has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Deleting Data Cancled!!");
                        }
                    });
            })

        })
    </script>
@endsection
