@extends('dashboard::newLayouts.master')
@section('title')
    Create Purchase Transactions
@endsection
@section('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<style>

    .addtionalfield {
        width:90%
    }
</style>
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('purchase.index') }}">Purchases</a></li>
                <li class="breadcrumb-item active">Create Purchase</li>
            </ol>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Create Purchase</h4>
                {!! Form::open(['route' => 'purchase.store', 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate','id'=>'formValidation','autocomplete'=>'off']) !!}
                @include('inventory::purchase.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
<script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script>
<script src="{{Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#customer_id').select2({
            });
        jQuery("#aniversary_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
        jQuery("#birth_datepicker").datepicker({autoclose:!0,todayHighlight:!0});


        var addButton = $('.add_button'); //Add button selector
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var wrapperCus = $('.customerField'); 
        var divToRemove = $('.divToRemove'); 
        var fieldHTML = '<hr><div class="addtionalfield form-row"><label for="inputEmail3" class="col-sm-3 col-form-label">Amount<span class="redAstrick">*</span>:</label><div class="col-sm-9"><input type="number" class="form-control" id="amount" name="amount[]" @error('amount') is-invalid @enderror" placeholder="Amount" required><div class="clearfix"></div>@if ($errors->has('amount'))<span class="text-danger">{{ $errors->first('amount') }}</span>@endif</div><label for="inputEmail3" class="col-sm-3 col-form-label">Bill No.<span class="redAstrick">*</span>:</label><div class="col-sm-9  row"><input type="text" name="bill_no[]" class="form-control col-sm-11" placeholder="Enter Bill No."/><a href="javascript:void(0);" class="remove_button col-sm-1"><i class="sidenav-icon feather icon-minus-circle"></i></a></div></div>'; //New input field html 

        
        var CusHTML = '<div class="divToRemove"><div class="col-md-6"><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Full Name <span class="redAstrick">*</span>:</label><div class="col-sm-8"><input type="text"  name="name" class="form-control" id="name" placeholder="Full Name" required></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Choose Membership<span class="redAstrick"> *</span>:</label> <div class="col-sm-8"></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Email<span class="redAstrick"> *</span>:</label><div class="col-sm-8"><input type="email" name="email" class="form-control" id="email" placeholder="Email" ></div></div><div class="form-group row "><label class="col-form-label col-sm-4">Aniversary Date:</label><div class="col-sm-8"><div class="input-group"><input type="text" class="form-control" placeholder="mm/dd/yyyy" id="aniversary_datepicker"name="aniversary_date"><div class="input-group-append"><span class="input-group-text"><i class="ti-calendar"></i></span></div></div></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Spouse Name:</label><div class="col-sm-8"><input type="text" name="spouse_name" class="form-control" id="spouse_name" placeholder="Spouse Name" ></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Phone:</label><div class="col-sm-8"><input type="text" name="mobile" class="form-control" id="mobile" placeholder="Spouse Name" data-parsley-type="number" data-parsley-trigger="keyup" ></div></div></div><div class="col-md-6"><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Mobile<span class="redAstrick">*</span>:</label><div class="col-sm-8"><input type="text" name="phone" class="form-control" id="phone" placeholder="Mobile" data-parsley-type="number" data-parsley-trigger="keyup"></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Address<span class="redAstrick">*</span>:</label><div class="col-sm-8"><input type="text" name="address" class="form-control" id="address" placeholder="Address" ></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Designation:</label><div class="col-sm-8"><input type="text" name="designation" class="form-control" id="designation" placeholder="Designation" ></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Birth Date:</label><div class="col-sm-8"><div class="input-group"><input type="text" name="birth_date" class="form-control" id="birth_datepicker" placeholder="Birth Date - mm/dd/yyyy" ><div class="input-group-append"><span class="input-group-text"><i class="ti-calendar"></i></span></div></div></div></div><div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Organization/Company:</label><div class="col-sm-8"><input type="text" name="company" class="form-control" id="company" placeholder="Organization/Company Name" ></div></div></div><br><br><br><div>';
        
        //Once add button is clicked
        $(addButton).click(function(){
                
                $(wrapper).append(fieldHTML); //Add field html
            
        });
        
        //Once remove button is clicked
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            // $(this).parent('div').remove();
            $('.addtionalfield').remove();
        });
        $(document).on('change','#customer_id',function(){
            var customer=$('#customer_id').val();
            if (customer!=='') {
               
                $('#customer_id').prop('required',true);
                // $(wrapperCus).append(CusHTML);
                $(divToRemove).remove();
            }else{
                
                $(wrapperCus).append(CusHTML);
                $('#customer_id').prop('required',false);
                // $(wrapperCus).remove();
            }

        })
       
    });
    </script>
@endsection
