{{-- @dd($customer) --}}
<div class="row">
    <div class="col-md-12">

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 col-form-label" style="background: rgb(162, 162, 248)">Choose Customer <span class="redAstrick"> *</span>:</label>
            <div class="col-sm-9">

                {{-- {!! form::select('customer_id', $customers, null, [
                    'class' => 'form-control',
                    'id' => 'customer_id',
                    'placeholder' => 'Choose Customer',
                ]) !!} --}}
        <select class="form-control" name="customer_id" id="customer_id" placeholder="Choose Customer" daa-role="tagsinput" required>
            <option value="">--Select Customer--</option>
            @foreach ($customers as $customer)
            <option value="{{$customer->id}}">{{$customer->name}}
             <br>->Card No :{{$customer->card->card_no}}</option>
            @endforeach
       </select>

            </div>
        </div>
        <div >
            <h5 style="background: rgb(162, 162, 248)">Or Fill The Form</h5>
            <br>
        </div>
        <div class=" customerField">
            <div class="divToRemove row">
                <div class="col-md-6">
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Full Name <span
                                class="redAstrick">*</span>:</label>
                        <div class="col-sm-8"><input type="text" name="name" class="form-control" id="name"
                                placeholder="Full Name" required></div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Choose
                            Membership<span class="redAstrick"> *</span>:</label>
                        <div class="col-sm-8">
                            {!! form::select('membership_id', $memberships, null, ['class' => 'form-control', 'id' => 'membership_id', 'placeholder' => 'Choose Membership', 'required']) !!}
                        </div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Email<span
                                class="redAstrick"> *</span>:</label>
                        <div class="col-sm-8"><input type="email" name="email" class="form-control" id="email"
                                placeholder="Email" ></div>
                    </div>
                    <div class="form-group row "><label class="col-form-label col-sm-4">Aniversary Date:</label>
                        <div class="col-sm-8">
                            <div class="input-group"><input autocomplete="false" type="text" class="form-control" placeholder="mm/dd/yyyy"
                                    id="aniversary_datepicker"name="aniversary_date">
                                <div class="input-group-append"><span class="input-group-text"><i
                                            class="ti-calendar"></i></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Spouse
                            Name:</label>
                        <div class="col-sm-8"><input type="text" name="spouse_name" class="form-control"
                                id="spouse_name" placeholder="Spouse Name" ></div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Phone:</label>
                        <div class="col-sm-8"><input type="text" name="mobile" class="form-control" id="mobile"
                                placeholder="Spouse Name" data-parsley-type="number" data-parsley-trigger="keyup"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Mobile<span
                                class="redAstrick">*</span>:</label>
                        <div class="col-sm-8"><input type="text" name="phone" class="form-control" id="phone"
                                placeholder="Mobile" data-parsley-type="number" data-parsley-trigger="keyup"></div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Address<span
                                class="redAstrick">*</span>:</label>
                        <div class="col-sm-8"><input type="text" name="address" class="form-control" id="address"
                                placeholder="Address"></div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3"
                            class="col-sm-4 col-form-label">Designation:</label>
                        <div class="col-sm-8"><input type="text" name="designation" class="form-control"
                                id="designation" placeholder="Designation"></div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3" class="col-sm-4 col-form-label">Birth
                            Date:</label>
                        <div class="col-sm-8">
                            <div class="input-group"><input type="text" name="birth_date" class="form-control"
                                    id="birth_datepicker" autocomplete="false" placeholder="Birth Date - mm/dd/yyyy">
                                <div class="input-group-append"><span class="input-group-text"><i
                                            class="ti-calendar"></i></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row"><label for="inputEmail3"
                            class="col-sm-4 col-form-label">Organization/Company:</label>
                        <div class="col-sm-8"><input type="text" name="company" class="form-control"
                                id="company" placeholder="Organization/Company Name"></div>
                    </div>
                </div><br><br><br>
                
                </div>
                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">Amount<span class="redAstrick">
                            *</span>:</label>
                    <div class="col-sm-9">
                        {{-- {!! Form::text('amount', null, [
                            'class' => 'form-control',
                            'id' => 'amount',
                            'placeholder' => 'Amount',
                            'required',
                        ]) !!} --}}
                        <input type="number" class="form-control" id="amount" name="amount[]" @error('amount') is-invalid @enderror" placeholder="Amount" required>
                        <div class="clearfix"></div>
                       @if ($errors->has('amount'))
                        <span class="text-danger">{{ $errors->first('amount') }}</span>
                    @endif
                    </div>
                </div>
                {{-- <div class="form-group row field_wrapper">
            <label for="inputEmail3" class="col-sm-3 col-form-label">Bill No.<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-9">
                {!! Form::text('bill_no[]', null, ['class' => 'form-control', 'id' => 'amount', 'placeholder' => 'Amount', 'required']) !!}
            </div>
        </div> --}}
                <div class="form-group row">
                    {{-- <div>
                <input type="text" name="field_name[]" value=""/>
                <a href="javascript:void(0);" class="add_button" title="Add field"><img src="add-icon.png"/></a>
            </div> --}}

                    <label for="inputEmail3" class="col-sm-3 col-form-label">Bill No.<span class="redAstrick">
                            :</label>
                    <div class="col-sm-9 field_wrapper row ml-0">
                        {{-- {!! Form::text('bill_no[]', null, ['class' => 'form-control', 'id' => 'amount', 'placeholder' => 'Amount', 'required']) !!} --}}
                        <input type="text" name="bill_no[]" class="form-control col-sm-8"
                            placeholder="Enter Bill No." />
                        <a href="javascript:void(0);" class="add_button col-sm-4" title="Add field"><i
                                class="sidenav-icon feather icon-plus-circle"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group mb-0 justify-content-end row">
                    <div class="col-sm-9">
                        <button type="submit"
                            class="btn btn-info waves-effect waves-light float-right">Submit</button>
                    </div>
                </div>
            </div>
        </div>
