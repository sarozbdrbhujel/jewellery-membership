@extends('dashboard::newLayouts.master')
@section('title')
    Purchase History
@endsection
@section('style')
    <!-- third party css -->
    <link href="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/select.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <!-- third party css end -->
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item active">Purchases</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box table-responsive">
                <h4 class="mt-0 header-title">Purchases History</h4>
                

                <table id="responsive-customers" class="table table-bordered table-bordered  nowrap">
                    <a class="btn btn-sm btn-info float-right" href="{{ route('purchase.create') }}"> <i
                            class="fa fa-plus"> </i> </a>

                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Customer Name</th>
                            <th>Card No.</th>
                            <th>Customer Phone</th>
                            <th>Bill</th>
                            <th>Amount</th>
                            {{-- <th>Action</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($purchase as $purchase)
                            @foreach ( $purchase->details as $item)
                                
                            
                        {{-- @dd($purchase) --}}
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $purchase->customer->name }}</td>
                                <td>{{ !is_null($purchase->customer->card)?$purchase->customer->card->card_no:null }}</td>
                                <td>{{ $purchase->customer->phone }}</td>
                                <td>{{ $item->bill_nos }}</td>
                                <td>Rs. {{ $item->amount }}</td>
                                {{-- <td> --}}
                                    {{-- <a href="{{ route('purchase.show', $purchase->id) }}" class="btn btn-sm btn-success"><i
                                            class="fa fa-eye"></i></a> --}}
                                    {{-- <a class="btn btn-info btn-sm " href="{{route('purchase.edit',$purchase->id)}}"
                                        >
                                        <i class="fa fa-edit">
                                        </i>
                                    </a> --}}
                                    {{-- <a href="javascript:void(0);" class="btn btn-sm btn-danger delete"
                                                data-id="{{ $purchase->id }}"><i class="fas fa-trash"></i></a>
                                            &nbsp; &nbsp;
                                            <form class="postdestroy" id="form_{{ $purchase->id }}" style="margin: 0;"
                                                method="Post" action="{{ route('purchase.destroy', $purchase->id) }}"
                                                data-toggle="modal" data-target="#exampleModal">
                                                @csrf
                                                @method('Delete')
                                            </form> --}}



                                {{-- </td> --}}
                            </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    </div> <!-- end row -->
@endsection
@section('script')
    <!-- third party js -->
    <script src="{{ Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!-- third party js ends -->
    <script>
        $(document).ready(function() {

            // $('#responsive-customers').DataTable();
            if (!$.fn.DataTable.isDataTable('#datatable-buttons')) {
             $('#responsive-customers').DataTable({
                        // dom: 'Bfrtip',
                        dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
                            "<'row'<'col-sm-12'tr>>" +
                            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                        scrollX: true,
                        "order": [
                            // [0, "desc"]
                        ],
                        buttons: [{
                                extend: 'collection',
                                text: '<i class="fa fa-file"></i> Export As',
                                className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light mb-2 mt-1 dropdown-toggle',
                                buttons: [
                                    // className : 'mt-1',
                                    // 'copy', 'csv', 'excel', 'pdf', 'print',
                                    {
                                        extend: 'pdf',
                                        // text: 'Download',
                                        text: '<i class="fa fa-file"></i> Download',
                                        className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                        key: {
                                            key: 'c',
                                            altKey: true
                                        },
                                        exportOptions: {
                                            // columns: [9,10]
                                            columns: [ ':visible']
                                        }
                                    },
                                    {
                                        extend: 'csv',
                                        // text: 'Download',
                                        text: '<i class="fa fa-file"></i> Excel',
                                        className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                        key: {
                                            key: 'x',
                                            altKey: true
                                        },
                                        exportOptions: {
                                            // columns: [9,10]
                                            columns: [ ':visible']
                                        }
                                    },
                                ]
                            }
    
                        ],
                        
    
                    });
                }
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this data!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your record has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Deleting Data Cancled!!");
                        }
                    });
            })

        })
    </script>
@endsection
