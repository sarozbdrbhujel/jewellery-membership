{{-- @dd($customer) --}}
<div class="row">
    <div class="col-md-12">

        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 col-form-label">Choose Gift<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-9">
                
                {!! form::select('reward_id', $rewards, null, ['class' => 'form-control', 'id' => 'reward_id', 'placeholder' => 'Choose Gift', 'required']) !!}


            </div>
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-3 col-form-label">Customer<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-9">
                {!! form::select('customer_id', $customers, null, ['class' => 'form-control', 'id' => 'customer_id', 'placeholder' => 'Customer', 'required']) !!}
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group mb-0 justify-content-end row">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-info waves-effect waves-light float-right" id="submitBtn">Submit</button>
            </div>
        </div>
    </div>
</div>