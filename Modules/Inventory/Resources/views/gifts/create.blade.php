@extends('dashboard::newLayouts.master')
@section('title')
    Create Customer Gifts
@endsection
@section('style')
{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
type="text/css" /> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css" integrity="sha512-6S2HWzVFxruDlZxI3sXOZZ4/eJ8AcxkQH1+JjSe/ONCEqR9L4Ysq5JdT5ipqtzU7WHalNwzwBv+iE51gNHJNqQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<style>
    .toast-message{
        color:black;
    }
</style>
@endsection

@section('content')
    {{-- @dd($customers) --}}
    <div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('gifts.index') }}">Gifts</a></li>
                <li class="breadcrumb-item active">Issue Customer Gifts</li>
            </ol>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Gifts</h4>
                {!! Form::open(['route' => 'gifts.store', 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate', 'id' => 'formValidation']) !!}
                @include('inventory::gifts.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js" integrity="sha512-lbwH47l/tPXJYG9AcFNoJaTMhGvYWhVM9YI43CT+uteTRRaiLCui8snIgyAN8XWgNjNhCqlAUdzZptso6OCoFQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
            // $(document).on('click', '#submitBtn', function() {
            //     var customer = $('#customer_id').val();
            //     var reward = $('#reward_id').val();
            //     $.ajax({
            //         type: "post",
            //         url: "{{ route('gifts-check') }}",
            //         data: {
            //             'customer_id': customer,
            //             'reward_id': reward,
            //             '_token' : "{{ csrf_token() }}",
            //         },
            //         context: this,
            //         success: function(response) {
            //             console.log(response)
            //             if (response.error) {
            //                 toastr["error"](response.error)
            //             }
            //             if (response.success) {
            //                 $('#formValidation').submit()
            //             }
            //         },
            //         errors: function(error) {

            //         },

            //     });
            // })
        })
    </script>
@endsection
