@extends('dashboard::newLayouts.master')
@section('title')
    Customer Details- {{ $customer->name }}
@endsection
@section('content')
    {{-- @dd($customer) --}}
    <div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item "><a href="{{ route('dashboard.index') }}">Customers</a></li>
                <li class="breadcrumb-item active">{{ $customer->name }}</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box task-detail">
                
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Customer Name</h5>
                        <p> {{ $customer->name }} <small class="text-muted"></small></p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Phone No.</h5>
                        <p> {{ $customer->phone }} <small class="text-muted"></small></p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Email</h5>
                        <p>{{ $customer->email }}</p>
                    </div>

                </div>
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Address</h5>
                        <p> {{ $customer->address }}
                            <small class="text-muted"></small>
                        </p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Aniversary Date</h5>
                        <p>{{ $customer->customerDetails->aniversary_date }} <small class="text-muted"></small>
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Spouse Name</h5>
                        <p> {{ $customer->customerDetails->spouse_name }} <small class="text-muted">g</small></p>
                    </div>
                </div>
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Birth Date</h5>
                        <p> {{ $customer->customerDetails->birth_date }}
                            <small class="text-muted"></small>
                        </p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Mobile</h5>
                        <p> {{ $customer->customerDetails->mobile }}</p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Designation</h5>
                        <p>{{ $customer->customerDetails->designation }}</p>
                    </div>
                </div>

                <hr>
                <div class="form-group mb-0 justify-content-end row">
                    <div class="col-sm-9">
                        <a href="{{route('customers.index')}}" class="btn btn-info waves-effect waves-light float-right">Back</a>
                        <a href="{{route('customers.edit',$customer->id)}}" class="btn btn-warning waves-effect waves-light float-right mr-2" style="color:black;">Update</a>
                    </div>
                </div>

            </div>
        </div><!-- end col -->

    </div>
    </div>
    <!-- end row -->
@endsection
