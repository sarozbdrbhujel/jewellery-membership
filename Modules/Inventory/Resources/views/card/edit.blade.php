@extends('dashboard::layouts.master')
@section('title')
    Customer Create
@endsection
@section('content')
{{-- @dd($customer) --}}
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('customers.index') }}">Customers</a></li>
                <li class="breadcrumb-item active">Create Customer</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Edit Customer</h4>
                {!! Form::open(['route'=>['customers.update',$customer->id], 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate','id'=>'formValidation']) !!}
                @method('PATCH')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Full Name <span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" placeholder="Full Name" name="name" required value="{{$customer->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Email<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" placeholder="Email" name="email" required value="{{$customer->email}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Aniversary Date:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="aniversary_date" placeholder="Aniversary Date" name="aniversary_date" value="{{$customer->customerDetails->aniversary_date}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Spouse Name:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="spouse_name" placeholder="Spouse Name" name="spouse_name" value="{{$customer->customerDetails->spouse_name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Mobile:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="mobile" placeholder="Mobile" name="mobile"  value="{{$customer->customerDetails->mobile}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Phone<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phone" placeholder="Phone" name="phone" required   value="{{$customer->phone}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Address<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="address" placeholder="Address" name="address" required  value="{{$customer->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Birth Date:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="birth_date" placeholder="Birth Date" name="birth_date" value="{{$customer->customerDetails->birth_date}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Designation:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="designation" placeholder="Designation" name="designation" value="{{$customer->customerDetails->designation}}">
                            </div>
                        </div>
                        <div class="form-group mb-0 justify-content-end row">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info waves-effect waves-light float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
@endsection
