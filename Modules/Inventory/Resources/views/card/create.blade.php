@extends('dashboard::newLayouts.master')
@section('title')
    Create Member Card
@endsection
@section('content')
{{-- @dd($customers) --}}
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('cards.index') }}">Member Card</a></li>
                <li class="breadcrumb-item active">Create Member Card</li>
            </ol>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Card Details</h4>
                {!! Form::open(['route' => 'cards.store', 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate','id'=>'formValidation']) !!}
                @include('inventory::card.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
@endsection
