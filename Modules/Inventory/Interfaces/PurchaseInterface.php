<?php
namespace Modules\Inventory\Interfaces;

interface PurchaseInterface{
    // public function get($pagination = null, $orderBy = null, $limit = null);
    public function getAll();
    // public function find($id);
    public function findOrFail($id);
    public function delete($id);
    public function store($request = []);
    public function update($request = [], $id) ;
    public function pluck($id, $name) ;

}