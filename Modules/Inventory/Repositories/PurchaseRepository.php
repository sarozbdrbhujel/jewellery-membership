<?php
namespace Modules\Inventory\Repositories;
use Modules\Inventory\Entities\Purchase;
use Modules\Inventory\Interfaces\PurchaseInterface;

class PurchaseRepository implements PurchaseInterface{
    public function __construct(Purchase $purchase)
    {
        $this->purchase = $purchase;
    }

    public function getAll($orderBy = null)
    {
        return $this->purchase->get();
    }

    public function findOrFail($id)
    {
        return $this->purchase->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->purchase->find($id);
        return $this->purchase->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->purchase->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->purchase->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->purchase->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->purchase->whereHas($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->purchase->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->purchase->pluck($id, $name);
    }


}