<?php
namespace Modules\Inventory\Repositories;
use Modules\Inventory\Entities\Card;
use Modules\Inventory\Interfaces\CardInterface;

class CardRepository implements CardInterface{
    public function __construct(Card $card)
    {
        $this->card = $card;
    }

    public function getAll($orderBy = null)
    {
        return $this->card->get();
    }

    public function findOrFail($id)
    {
        return $this->card->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->card->find($id);
        return $this->card->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->card->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->card->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->card->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->card->whereHas($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->card->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->card->pluck($id, $name);
    }


}