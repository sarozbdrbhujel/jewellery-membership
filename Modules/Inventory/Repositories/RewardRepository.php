<?php
namespace Modules\Inventory\Repositories;
use Modules\Inventory\Entities\Reward;
use Modules\Inventory\Interfaces\RewardInterface;

class RewardRepository implements RewardInterface{
    public function __construct(Reward $reward)
    {
        $this->reward = $reward;
    }

    public function getAll($orderBy = null)
    {
        return $this->reward->get();
    }

    public function findOrFail($id)
    {
        return $this->reward->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->reward->find($id);
        return $this->reward->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->reward->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->reward->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->reward->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->reward->whereHas($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->reward->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->reward->pluck($id, $name);
    }


}