<?php
namespace Modules\Inventory\Repositories;
use Modules\Inventory\Entities\Membership;
use Modules\Inventory\Interfaces\MembershipInterface;

class MembershipRepository implements MembershipInterface{
    public function __construct(Membership $membership)
    {
        $this->membership = $membership;
    }

    public function getAll($orderBy = null)
    {
        return $this->membership->get();
    }

    public function findOrFail($id)
    {
        return $this->membership->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->membership->find($id);
        return $this->membership->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->membership->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->membership->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->membership->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->membership->whereHas($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->membership->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->membership->pluck($id, $name);
    }


}