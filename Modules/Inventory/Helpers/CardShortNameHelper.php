<?php
namespace Modules\Inventory\Helpers;
use Modules\Inventory\Entities\Card;
use Modules\Inventory\Entities\Membership;
use Modules\Customer\Entities\Customer;

class CardShortNameHelper {
    
    public static function uniqucardShortName($membership_id,$customer_id){

        $membership=Membership::withCount('cards')->findOrFail($membership_id);
        $membershipShortName=str_replace("-","",$membership->short_name);

        $customer=Customer::findOrFail($customer_id);
        $customerName=trim(ucwords($customer->name));
        $words= preg_split('/\s+/', $customerName);
        $customerInitialNames="";
        foreach($words as $w){
            $customerInitialNames .=$w[0];
        }
        $cardShortName=$membershipShortName.($membership->cards_count+1).$customerInitialNames;
        // dd($cardShortName);
        return $cardShortName;
        
    }
}