<?php
namespace Modules\Inventory\Helpers;
use Modules\Inventory\Entities\Membership;
class MembershipShortNameHelper{
    
    public static function uniquemembershipShortName($string){
        
        $string=trim(ucwords($string));
        // $words= explode("  ", $string);
        //explode(" ",$string ) only take into consideration of signle space so useing preg_split
        $words= preg_split('/\s+/', $string);
        $firstLetters="";
        foreach($words as $w){
            $firstLetters .=$w[0];
        }
        $status=Membership::where('short_name',$firstLetters)->first();
        if(is_null($status)){
            $shortName= $firstLetters.'-PC';
            return $shortName;

        }else{
            $count=Membership::where('short_name','like','%'.$firstLetters)->count();
           
            $shortName=$firstLetters.'-PC'.$count;
            return $shortName;
        }
    }
}