<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::prefix('inventory')->group(function() {
Route::group(['prefix'=>'inventory',
'middleware'=>['auth']
],function(){
    Route::resource('purchase','InventoryController');
    Route::post('create-customer-card','CardController@createCustomerCard')->name('create-customer-card');
    Route::resource('membership','MembershipController');
    Route::resource('rewards','RewardController');
    Route::resource('cards','CardController');
    Route::resource('gifts','GiftController');
    Route::post('gifts-check','GiftController@checkCustomerGift')->name('gifts-check');
    Route::post('membership-check','CardController@membershipCheck')->name('membership-check');
});
