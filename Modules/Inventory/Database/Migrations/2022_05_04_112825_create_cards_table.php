<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cards', function (Blueprint $table) {
            $table->id();
            $table->boolean('status')->default(false);
            $table->string('card_no');
            $table->unsignedBigInteger('authorized_by');
            $table->unsignedBigInteger('membership_id');
            $table->unsignedBigInteger('customer_id');
            $table->string('rewards_point')->nullable();
            $table->longText('other_informtions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }
}
