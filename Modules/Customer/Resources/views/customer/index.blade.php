@extends('dashboard::newLayouts.master')
@section('title')
    Customers || Shree Ganapati Jewellers
@endsection
@section('style')
    <!-- third party css -->
    <link href="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
    <!-- third party css end -->
    {{-- <link href="{{ Module::asset('dashboard:libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" /> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        input::-webkit-calendar-picker-indicator,
        input::-webkit-inner-spin-button {
            /* display: none; */
            -webkit-appearance: none;
        }

        input[type="date"]::-webkit-calendar-picker-indicator {
            /* display: none; */
            /* -webkit-appearance: none; */
        }

        div.dt-button-collection {
            position: absolute;
            z-index: 2001;
            background: #5bc0de;
        }

        .dt-button-collection .dropdown-menu {
            background-color: #5bc0de;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5bc0de;
        }

        .select2-container {
            width: 70% !important;
        }
    </style>
@endsection
@section('content')
    <div class="container-fluid flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-12">

                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                    <li class="breadcrumb-item active">Customers</li>
                </ol>
            </div>
            <div class="col-12">
                <div class="card-box table-responsive">
                    <h4 class="mt-0 header-title">Customers List</h4>
                    <div>
                        {{-- <form method="POST" action="{{route('imports')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                
                                <div class="col-md-12 mb-3 mt-3">
                                    <p>Please Upload CSV in Given Format <a href="{{ asset('files/sample-data-sheet.csv') }}" target="_blank">Sample CSV Format</a></p>
                                </div>
                                <div class="col-sm-12 mb-3 mt-3 mb-sm-0">
                                    <span style="color:red;">*</span>File Input(Datasheet)</label>
                                    <input 
                                        type="file" 
                                        class="form-control form-control-user @error('file') is-invalid @enderror" 
                                        id="exampleFile"
                                        name="file" 
                                        value="{{ old('file') }}">
            
                                    @error('file')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
            
                            </div>
                        </div>
            
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-user float-right mb-3">Upload Customers</button>
                            
                        </div>
                    </form> --}}
                        <form method="POST" action="{{ route('customers-filter') }}" class="form-inline mb-1"
                            autocomplete="off">
                            @csrf
                            {{-- &nbsp;{!! Form::label('card no', 'Customer') !!} &nbsp; --}}
                            <div class="form-group">

                                <label for="">Customer</label>
                                {!! Form::select('customer_id', $cust, $customer_id, [
                                    'class' => 'js-example-basic-multiple form-control  selectWidth metallist ',
                                    'placeholder' => 'Customer',
                                ]) !!}
                            </div>
                            <div class="form-group">
                                {{-- {!! Form::label('card no', 'Card Type') !!} --}}
                                <label for="">Card Type</label>
                                {!! Form::select('card_type', $memberships, $card_type, [
                                    'class' => 'js-example-basic-multiple form-control  selectWidth metallist ',
                                    'placeholder' => 'Card Type',
                                ]) !!}
                            </div>
                            <br>
                            <br>
                            <div class="form-group">
                                <label for="">Filter By Date</label> ::
                                <div class="input-group">
                                    &nbsp;{!! Form::label('from', 'From') !!} &nbsp;
                                    {!! Form::text('from_date', $from_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'fromDate',
                                        'placeholder' => 'Form Date',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>


                                <div class="input-group">
                                    {!! Form::label('to', 'To') !!} &nbsp;
                                    {!! Form::text('to_date', $to_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'toDate',
                                        'placeholder' => 'To Date',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Filter By Age</label>::
                                <div class="input-group">
                                    &nbsp;{!! Form::label('from', 'From') !!} &nbsp;
                                    {!! Form::text('from_age', $from_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'fromDate',
                                        'placeholder' => 'Form Age',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>

                                <div class="input-group">
                                    {!! Form::label('to', 'To') !!} &nbsp;
                                    {!! Form::text('to_age', $to_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'toDate',
                                        'placeholder' => 'To Age',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Filter By Amount</label>::
                                <div class="input-group">
                                    &nbsp;{!! Form::label('from', 'From') !!} &nbsp;
                                    {!! Form::text('from_amt', $from_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'fromAmount',
                                        'placeholder' => 'Form Amount',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>

                                <div class="input-group">
                                    {!! Form::label('to', 'To') !!} &nbsp;
                                    {!! Form::text('to_amt', $to_date, [
                                        'class' => 'form-control float-left selectWidth placehold',
                                        'id' => 'toAmt',
                                        'placeholder' => 'To Amount',
                                    ]) !!}
                                    <div class="input-group-append">
                                        <span class="input-group-text fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>



                            <button type="submit" class="btn btn-sm btn-info ml-1 float-left"> <i class="mdi mdi-filter">
                                </i>
                                Filter </button>
                        </form>
                    </div>
                    <div class="form-group">
                        {!! Form::label('to', 'Columns :') !!} &nbsp;
                        <select class="select2 select2-multiple" multiple="multiple" multiple data-placeholder="Choose ..."
                            id="hideOthers">
                            <option value="0">ID</option>
                            <option value="1">Member No.</option>
                            <option value="2">Card Type</option>
                            <option value="3">Name</option>
                            <option value="4">Phone</option>
                            <option value="5">Email</option>
                            <option value="6">Total Amount</option>
                            <option value="7">Reward Points</option>
                            <option value="8">Gift</option>
                            <option value="9">Action</option>

                        </select>
                        <button class="btn btn-sm btn-primary" id="hideOthersBtn">Search</button>
                    </div>

                    <table id="responsive-customers" class="table table-bordered table-bordered  nowrap">

                        <a class="btn btn-sm btn-info float-right" href="{{ route('customers.create') }}"> <i
                                class="fa fa-plus"> </i> </a>


                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Member No.</th>
                                <th>Card Type</th>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Added Date</th>
                                <th>Total Amount</th>
                                <th>Reward Points</th>
                                <th>Gifts</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cusotmers as $customer)
                                @php
                                    $total = 0;
                                    foreach ($customer->purchases as $purchase) {
                                        foreach ($purchase->details as $key => $value) {
                                            # code...
                                            $total = $total + $value->amount;
                                        }
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $loop->index + 1 }}</td>
                                    <td>
                                        @if (!is_null($customer->card))
                                            {{ $customer->card->card_no }}
                                        @else
                                            <a class="btn btn-xs waves-effect waves-light" data-toggle="modal"
                                                data-id="{{ $customer->id }}" id="modelBtnMembership">
                                                <span class="badge badge-danger">Not a Member Yet</span></a>
                                        @endif

                                    </td>
                                    <td>
                                        @if (!is_null($customer->card))
                                            {{ !is_null($customer->card->membership)?$customer->card->membership->short_name:null }}
                                        @else
                                            <span class="badge badge-danger"><i class="mdi mdi-close-thick"></i></span>
                                        @endif
                                    </td>
                                    <td>{{ $customer->name }}</td>
                                    <td>{{ $customer->phone }}</td>
                                    <td>{{ $customer->email }}</td>
                                    <td>{{ !is_null( $customer->birth_date)?Carbon\Carbon::parse($customer->added_date)->format('Y-m-d'):null }}</td>
                                    <td>Rs. {{ $total }}</td>
                                    <td>
                                        @if (!is_null($customer->card))
                                            {{ number_format($customer->card->rewards_point, 0) }}
                                        @else
                                            <span class="badge badge-danger"><i class="mdi mdi-close-thick"></i></span>
                                        @endif
                                    </td>
                                    <td>
                                        @if (!is_null($customer->card))
                                            @if (!is_null($customer->rewards->first()))
                                                <a class="btn btn-sm waves-effect waves-light" data-toggle="modal"
                                                    data-id="{{ $customer->id }}" id="modelBtnGift">
                                                    <span
                                                        class="badge badge-success ">{{ $customer->rewards->first()->name }}</span>
                                                </a>
                                            @else
                                                <a class="btn btn-xs waves-effect waves-light" data-toggle="modal"
                                                    data-id="{{ $customer->id }}" id="modelBtnGift">
                                                    <span class="badge badge-info"><i class="mdi mdi-close-thick">Not
                                                            Allocated</i></span></a>
                                            @endif
                                        @else
                                            <span class="badge badge-danger"><i class="mdi mdi-close-thick"></i></span>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- <a class="btn btn-secondary btn-xs waves-effect waves-light" data-toggle="modal"
                                            data-id="{{ $customer->id }}" id="modelBtn"><i class="fas fa-cart-plus"
                                                style="color:aliceblue;"></i></a> --}}

                                        <a href="{{ route('customers.show', $customer->id) }}"
                                            class="btn btn-xs btn-success"><i class="fa fa-eye"></i></a>

                                        <a class="btn btn-info btn-xs "
                                            href="{{ route('customers.edit', $customer->id) }}">
                                            <i class="fa fa-edit">
                                            </i>
                                        </a>

                                        @if (!is_null($customer->card))
                                            <a href="javascript:void(0);" class="btn btn-xs btn-danger delete"
                                                data-id="{{ $customer->card->id }}"><i class="fas fa-trash"></i></a>
                                            &nbsp; &nbsp;
                                            <form class="postdestroy" id="form_{{ $customer->card->id }}"
                                                style="margin: 0;" method="Post"
                                                action="{{ route('cards.destroy', $customer->card->id) }}"
                                                data-toggle="modal" data-target="#exampleModal">
                                                @csrf
                                                @method('Delete')
                                            </form>
                                        @endif



                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="d-flex justify-content-center">

                    {{ $cusotmers->links('pagination::bootstrap-4') }}
                </div>
            </div>
        </div> <!-- end row -->
    </div> <!-- end row -->
    
    {{-- purchase model --}}
    <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="purchaseModel">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Create Purchase</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {!! Form::open([
                    'route' => 'purchase.store',
                    'method' => 'post',
                    'class' => 'needs-validation form-horizontal',
                    'novalidate',
                    'id' => 'formValidation',
                ]) !!}
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Amount<span class="redAstrick">
                                *</span>:</label>
                        <div class="col-sm-9">
                            {!! Form::text('amount', null, [
                                'class' => 'form-control',
                                'id' => 'amount',
                                'placeholder' => 'Amount',
                                'required',
                            ]) !!}
                        </div>
                    </div>
                    <input type="text" name="customer_id" id="customerHidden" hidden>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- membership model --}}
    <div class="modal fade membership-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="membershipModel">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Create Membership Card</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {!! Form::open([
                    'route' => 'create-customer-card',
                    'method' => 'post',
                    'class' => 'needs-validation form-horizontal',
                    'novalidate',
                    'id' => 'membershipForm',
                ]) !!}
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Choose Membership<span
                                class="redAstrick"> *</span>:</label>
                        <div class="col-sm-9">
                            {!! form::select('membership_id', $memberships, null, [
                                'class' => 'form-control',
                                'id' => 'membership_id',
                                'placeholder' => 'Choose Membership',
                                'required',
                            ]) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Card NO. :</label>
                        <div class="col-sm-9">
                            {!! form::text('card_no', null, [
                                'class' => 'form-control',
                                'id' => 'card_no',
                                'placeholder' => 'Choose Membership',
                            ]) !!}
                        </div>
                    </div>
                    <input type="text" name="customer_id" id="customerHiddenMembership" hidden>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="membershipBtn">Submit</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    {{-- gift model --}}
    <div class="modal fade gift-modal-center" tabindex="-1" role="dialog" aria-labelledby="myCenterModalLabel"
        aria-hidden="true" style="display: none;" id="giftModel">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myCenterModalLabel">Allocate Reward</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                {!! Form::open([
                    'route' => 'gifts.store',
                    'method' => 'post',
                    'class' => 'needs-validation form-horizontal',
                    'novalidate',
                    'id' => 'giftform',
                ]) !!}
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-3 col-form-label">Choose Reward<span class="redAstrick">
                                *</span>:</label>
                        <div class="col-sm-9">
                            {!! form::select('reward_id', $rewards, null, [
                                'class' => 'form-control',
                                'id' => 'reward_id',
                                'placeholder' => 'Choose Reward',
                                'required',
                            ]) !!}
                        </div>
                    </div>
                    <input type="text" name="customer_id" id="customerHiddenGift" hidden>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="giftBtn">Submit</button>
                </div>
                {!! Form::close() !!}
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('script')
    <!-- third party js -->
    <script src="{{ Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    {{-- <script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <!-- third party js ends -->
    <script>
        $(document).ready(function() {

            $('.js-example-basic-multiple').select2({})
            $('.js-example-basic-multiple2').select2({})

        })
    </script>
    <script>
        $(document).ready(function() {
            if (!$.fn.DataTable.isDataTable('#datatable-buttons')) {
                var table = $('#responsive-customers').DataTable({
                    // dom: 'Bfrtip',
                    dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                    scrollX: true,
                    "order": [
                        // [0, "desc"]
                    ],
                    buttons: [{
                            extend: 'collection',
                            text: '<i class="fa fa-file"></i> Export As',
                            className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light mb-2 mt-1 dropdown-toggle',
                            buttons: [
                                // className : 'mt-1',
                                // 'copy', 'csv', 'excel', 'pdf', 'print',
                                {
                                    extend: 'pdf',
                                    // text: 'Download',
                                    text: '<i class="fa fa-file"></i> Download',
                                    className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                    key: {
                                        key: 'c',
                                        altKey: true
                                    },
                                    exportOptions: {
                                        // columns: [9,10]
                                        columns: [':visible']
                                    }
                                },
                                {
                                    extend: 'csv',
                                    // text: 'Download',
                                    text: '<i class="fa fa-file"></i> Excel',
                                    className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                    key: {
                                        key: 'x',
                                        altKey: true
                                    },
                                    exportOptions: {
                                        // columns: [9,10]
                                        columns: [':visible']
                                    }
                                },
                            ],
                            bPaginate: false,
                        }

                    ],
                    bPaginate: false,
                    showNEntries : false,
                    "paging":   false,
        "ordering": false,
        "info":     false
                    // "columnDefs": [
                    //     {
                    //         "targets": [2],
                    //         "visible": false,
                    //     },
                    //     {
                    //         "targets": [3],
                    //         "visible": false
                    //     }
                    // ]

                });
            }
            $(document).on('click', '#hideOthersBtn', function() {
                var hideOthers = $('#hideOthers').val();
                console.log(hideOthers);
                // e.preventDefault();

                // var column = table.column( $(this).attr('data-column') );

                // column.visible( ! column.visible() );
                table.columns([0, 1, 2, 3, 4, 5, 6, 7, 8, 9]).visible(false);
                // var datass=[0,1];
                table.columns(hideOthers).visible(true);
            })
            jQuery("#fromDate").datepicker({
                autoclose: !0,
                todayHighlight: !0,
                // format: 'dd/mm/yyyy',
                format: 'yyyy-mm-dd',
            });
            jQuery("#toDate").datepicker({
                autoclose: !0,
                todayHighlight: !0,
                format: 'yyyy-mm-dd',
            });
            // $('#responsive-customers').DataTable();
            $('#hideOthers').select2();
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
            @if (Session::has('membershipFound'))
                toastr.info('{{ Session::get('membershipFound') }}')
            @endif
            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this data!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your record has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Deleting Data Cancled!!");
                        }
                    });
            })
            $(document).on('click', '#modelBtn', function() {
                var customerId = $(this).data('id');
                $('#customerHidden').val(customerId);
                $('#purchaseModel').modal('show');
            })
            $(document).on('click', '#modelBtnMembership', function() {
                var customerId = $(this).data('id');
                $('#customerHiddenMembership').val(customerId);
                $('#membershipModel').modal('show');
            })
            $(document).on('click', '#modelBtnGift', function() {
                console.log('hello');
                var customerId = $(this).data('id');
                $('#customerHiddenGift').val(customerId);
                $('#giftModel').modal('show');
            })

            $('#giftform').parsley();
            $('#membershipForm').parsley();

            $(document).on('click', '#giftBtn', function() {
                var customer = $('#customerHiddenGift').val();
                var reward = $('#reward_id').val();
                console.log(customer, reward)
                // $.ajax({
                //     type: "post",
                //     url: "{{ route('gifts-check') }}",
                //     data: {
                //         'customer_id': customer,
                //         'reward_id': reward,
                //         '_token': "{{ csrf_token() }}",
                //     },
                //     context: this,
                //     success: function(response) {
                //         console.log(response)
                //         if (response.error) {
                //             toastr["error"](response.error)
                //         }
                //         if (response.success) {
                //             $('#giftform').submit()
                //         }
                //     },
                //     errors: function(error) {

                //     },

                // });
            })
            $(document).on('click', '#membershipBtn', function() {
                var card = $('#card_no').val();
                var customer = $('#customerHiddenMembership').val();
                var membership = $('#membership_id').val();
                console.log(card)
                $.ajax({
                    type: "post",
                    url: "{{ route('membership-check') }}",
                    data: {
                        'card_no': card,
                        'customer_id': customer,
                        'membership_id': membership,
                        '_token': "{{ csrf_token() }}",
                    },
                    // dataType: "dataType",
                    context: this,
                    success: function(response) {
                        console.log(response)
                        if (response.error) {
                            toastr["error"](response.error)
                        }
                        if (response.success) {
                            // alert('dkfjdskf');
                            $('#membershipForm').submit()
                        }
                    },
                    errors: function(error) {

                    },

                });
            })

        })
    </script>
@endsection
