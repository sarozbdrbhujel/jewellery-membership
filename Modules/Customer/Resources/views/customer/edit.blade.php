@extends('dashboard::newLayouts.master')
@section('title')
    Customer Edit
@endsection
@section('style')
    {{-- <link href="{{ Module::asset('dashboard:libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
<link href="{{ Module::asset('dashboard:libs/select2/select2.min.css')}}" rel="stylesheet" type="text/css" /> --}}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
{{-- @dd($customer) --}}
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        {{-- @dd($customer) --}}
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('customers.index') }}">Customers</a></li>
                <li class="breadcrumb-item active">Create Customer</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Edit Customer</h4>
                {!! Form::open(['route'=>['customers.update',$customer->id], 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate','id'=>'formValidation']) !!}
                @method('PATCH')
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Full Name <span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="name" placeholder="Full Name" name="name" required value="{{$customer->name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Choose Membership<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-8">
                                {!! form::select('membership_id', $memberships,  !is_null($customer->card->membership)?$customer->card->membership->id:null, ['class' => 'form-control', 'id' => 'membership_id', 'placeholder' => 'Choose Membership', 'required']) !!}
                            </div>
                            @if ($errors->has('membership_id'))
                                    <span class="text-danger">{{ $errors->first('membership_id') }}</span>
                                @endif
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Card No:</label>
                            <div class="col-sm-8">
                                {!! Form::text('card_no', !is_null($customer->card)?$customer->card->card_no:null, ['class' => 'form-control', 'id' => 'card', 'placeholder' => 'Card No.']) !!}
                            </div>
                            @if ($errors->has('email'))
                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Email<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" placeholder="Email" name="email"  value="{{$customer->email}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Aniversary Date:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="aniversary_datepicker" placeholder="Aniversary Date" name="aniversary_date" value="{{$customer->aniversary_date}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Spouse Name:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="spouse_name" placeholder="Spouse Name" name="spouse_name" value="{{$customer->spouse_name}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Mobile:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="mobile" placeholder="Mobile" name="mobile"  value="{{$customer->mobile}}">
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Phone<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phone" placeholder="Phone" name="phone"    value="{{$customer->phone}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Address<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="address" placeholder="Address" name="address"   value="{{$customer->address}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Choose Category<span class="redAstrick"> *</span>:</label>
                            <div class="col-sm-8">
                                {!! form::select('category_id[]', $category,!is_null( $customer->clientCategories)?$customer->clientCategories:null, ['class' => 'select2 select2-multiple js-example-basic-multiple', 'id' => 'category', 'data-placeholder' => 'Choose Category', 'required','multiple'>'multiple','multiple']) !!}
                            </div>
                            {{-- <select class="select2 select2-multiple js-example-basic-multiple" name="category_id[]"
                                multiple="multiple" required multiple data-placeholder="Choose ..." id="category">
                                @forelse ($category as $payroll)
                                    <option value="{{ $payroll->id }}" if> {{ $payroll->name }} </option>
                                @empty
                                @endforelse
                            </select> --}}
                            
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Birth Date:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="birth_datepicker" placeholder="Birth Date" name="birth_date" value="{{$customer->birth_date}}">
                            </div>
                        </div>
                        <div class="form-group row ">
                            <label class="col-form-label col-sm-4">Added Date:</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    {!! Form::text('added_date', $customer->added_date, ['class' => 'form-control', 'id' => 'added_datepicker', 'placeholder' => 'Added Date - mm/dd/yyyy']) !!}
                                    {{-- <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="aniversary_datepicker"
                                        name="aniversary_date"> --}}
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="ti-calendar"></i></span>
                                    </div>
                                </div><!-- input-group -->
                            </div>
                            @if ($errors->has('aniversary_date'))
                                    <span class="text-danger">{{ $errors->first('aniversary_date') }}</span>
                                @endif
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Designation:</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="designation" placeholder="Designation" name="designation" value="{{$customer->designation}}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-4 col-form-label">Organization/Company:</label>
                            <div class="col-sm-8">
                                {!! Form::text('company', null, ['class' => 'form-control', 'id' => 'designation', 'placeholder' => 'Organization/Company Name']) !!}
                            </div>
                            @if ($errors->has('company'))
                                    <span class="text-danger">{{ $errors->first('company') }}</span>
                                @endif
                        </div>
                        <div class="form-group mb-0 justify-content-end row">
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info waves-effect waves-light float-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
    </div>
@endsection
@section('script')
<script src="{{Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script>
<script>
    $(document).ready(function(){
        $('.js-example-basic-multiple').select2();
        jQuery("#aniversary_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
        jQuery("#added_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
        jQuery("#birth_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
    });
</script>
@endsection
