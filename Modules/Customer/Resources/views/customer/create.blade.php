@extends('dashboard::newLayouts.master')
@section('title')
    Customer Create
@endsection
@section('style')
{{-- <link href="{{Module::asset('dashboard:libs/bootstrap-timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet"> --}}
{{-- <link href="{{Module::asset('dashboard:libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css')}}" rel="stylesheet"> --}}
<link href="{{Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.css')}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
{{-- <link href="{{Module::asset('dashboard:libs/bootstrap-daterangepicker/daterangepicker.css')}}" rel="stylesheet"> --}}
{{-- <link href="{{Module::asset('dashboard:libs/multiselect/multi-select.css')}}"  rel="stylesheet" type="text/css" />
<link href="{{Module::asset('dashboard:libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" /> --}}
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item"><a href="{{ route('customers.index') }}">Customers</a></li>
                <li class="breadcrumb-item active">Create Customer</li>
            </ol>
        </div>
        {{-- @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif --}}
        <div class="col-12">
            <div class="card-box">
                <h4 class="mt-0 mb-3 header-title">Customer Details</h4>

                {{-- <form class="form-horizontal" role="form">
                    
                </form> --}}
                {{-- {!! Form::open(['route'=>'customers.store','method'=>'post','class'=>'needs-validation form-horizontal']) !!} --}}
                {!! Form::open(['route' => 'customers.store', 'method' => 'post', 'class' => 'needs-validation form-horizontal', 'novalidate', 'id' => 'formValidation','autocomplete'=>'off',]) !!}
                {{-- @csrf --}}
                @include('customer::customer.common')
                {!! Form::close() !!}
            </div> <!-- end card-box -->
        </div>
    </div>
</div>
@endsection
@section('script')
{{-- <script src="{{Module::asset('dashboard:libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js')}}"></script> --}}
        <script src="{{Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script>
        {{-- <script src="{{Module::asset('dashboard:libs/bootstrap-daterangepicker/daterangepicker.js')}}"></script> --}}
        {{-- <script src="{{Module::asset('dashboard:js/pages/form-advanced.init.js')}}"></script> --}}
        {{-- <script src="{{Module::asset('dashboard:libs/multiselect/jquery.multi-select.js')}}"></script>
        <script src="{{Module::asset('dashboard:libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script> --}}
        <script>
            $(document).ready(function(){
                $('.js-example-basic-multiple').select2();
                jQuery("#aniversary_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
                jQuery("#added_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
                jQuery("#birth_datepicker").datepicker({autoclose:!0,todayHighlight:!0});
            });
        </script>
@endsection
