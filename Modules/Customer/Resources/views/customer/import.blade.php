@extends('dashboard::newLayouts.master')
@section('title')
    Customers-Import || Shree Ganapati Jewellers
@endsection
@section('style')
    <!-- third party css -->
    <link href="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/datatables/select.bootstrap4.css') }}" rel="stylesheet"
        type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"
        type="text/css" />
    <link href="{{ Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.css') }}" rel="stylesheet">
    <!-- third party css end -->
    <link href="{{ Module::asset('dashboard:libs/select2/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        input::-webkit-calendar-picker-indicator,
        input::-webkit-inner-spin-button {
            /* display: none; */
            -webkit-appearance: none;
        }

        input[type="date"]::-webkit-calendar-picker-indicator {
            /* display: none; */
            /* -webkit-appearance: none; */
        }

        div.dt-button-collection {
            position: absolute;
            z-index: 2001;
            background: #5bc0de;
        }

        .dt-button-collection .dropdown-menu {
            background-color: #5bc0de;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5bc0de;
        }

        .select2-container {
            width: 70% !important;
        }

    </style>
@endsection
@section('content')
<div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item active">Customers Import</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box table-responsive">
                <h4 class="mt-0 header-title">Customers Import</h4>
                <div>
                    <form method="POST" action="{{route('imports')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                
                                <div class="col-md-12 mb-3 mt-3">
                                    {{-- <p>Please Upload CSV in Given Format <a href="{{ asset('files/sample-data-sheet.csv') }}" target="_blank">Sample CSV Format</a></p> --}}
                                    <p>Please Upload CSV in Given Format  CSV Format</p>
                                </div>
                                {{-- File Input --}}
                                <div class="col-sm-12 mb-3 mt-3 mb-sm-0">
                                    <span style="color:red;">*</span>File Input(Datasheet)</label>
                                    <input 
                                        type="file" 
                                        class="form-control form-control-user @error('file') is-invalid @enderror" 
                                        id="exampleFile"
                                        name="file" 
                                        value="{{ old('file') }}">
            
                                    @error('file')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
            
                            </div>
                        </div>
            
                        <div class="card-footer">
                            <button type="submit" class="btn btn-success btn-user float-right mb-3">Upload Customers</button>
                            
                        </div>
                    </form>
                    
                </div>
                
                    
                
            </div>
        </div>
    </div> <!-- end row -->
    </div> <!-- end row -->
    
@endsection
@section('script')
    <!-- third party js -->
    <script src="{{ Module::asset('dashboard:libs/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/pdfmake.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/pdfmake/vfs_fonts.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ Module::asset('dashboard:libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ Module::asset('dashboard:libs/select2/select2.min.js') }}"></script>
    <!-- third party js ends -->
    <script>
        $(document).ready(function() {
            if (!$.fn.DataTable.isDataTable('#datatable-buttons')) {
                var table =  $('#responsive-customers').DataTable({
                        // dom: 'Bfrtip',
                        dom: "<'row'<'col-sm-12 col-md-6'B><'col-sm-12 col-md-6'f>>" +
                            "<'row'<'col-sm-12'tr>>" +
                            "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
                        scrollX: true,
                        "order": [
                            // [0, "desc"]
                        ],
                        buttons: [{
                                extend: 'collection',
                                text: '<i class="fa fa-file"></i> Export As',
                                className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light mb-2 mt-1 dropdown-toggle',
                                buttons: [
                                    // className : 'mt-1',
                                    // 'copy', 'csv', 'excel', 'pdf', 'print',
                                    {
                                        extend: 'pdf',
                                        // text: 'Download',
                                        text: '<i class="fa fa-file"></i> Download',
                                        className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                        key: {
                                            key: 'c',
                                            altKey: true
                                        },
                                        exportOptions: {
                                            // columns: [9,10]
                                            columns: [ ':visible']
                                        }
                                    },
                                    {
                                        extend: 'csv',
                                        // text: 'Download',
                                        text: '<i class="fa fa-file"></i> Excel',
                                        className: 'btn btn-sm btn-info btn-rounded w-md waves-effect waves-light dropdown-item',
                                        key: {
                                            key: 'x',
                                            altKey: true
                                        },
                                        exportOptions: {
                                            // columns: [9,10]
                                            columns: [ ':visible']
                                        }
                                    },
                                ]
                            }
    
                        ],
                        // "columnDefs": [
                        //     {
                        //         "targets": [2],
                        //         "visible": false,
                        //     },
                        //     {
                        //         "targets": [3],
                        //         "visible": false
                        //     }
                        // ]
    
                    });
                }
            $(document).on('click','#hideOthersBtn',function(){
                var hideOthers=$('#hideOthers').val();
                console.log(hideOthers);
                // e.preventDefault();
        
                // var column = table.column( $(this).attr('data-column') );
        
                // column.visible( ! column.visible() );
                table.columns([0,1,2,3,4,5,6,7,8,9]).visible(false);
                // var datass=[0,1];
                table.columns(hideOthers).visible(true);
            })
            jQuery("#fromDate").datepicker({
                autoclose: !0,
                todayHighlight: !0,
                // format: 'dd/mm/yyyy',
                format: 'yyyy-mm-dd',
            });
            jQuery("#toDate").datepicker({
                autoclose: !0,
                todayHighlight: !0,
                format: 'yyyy-mm-dd',
            });
            // $('#responsive-customers').DataTable();
            $('#hideOthers').select2();
            @if (Session::has('message'))
                toastr.success('{{ Session::get('message') }}')
            @endif
            @if (Session::has('error'))
                toastr.warning('{{ Session::get('error') }}')
            @endif
            @if (Session::has('membershipFound'))
                toastr.info('{{ Session::get('membershipFound') }}')
            @endif
            $(document).on('click', '.delete', function() {
                id = $(this).data('id');
                swal({
                        title: "Are you sure?",
                        text: "Once deleted, you will not be able to recover this data!",
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
                            $('#form_' + id).submit();
                            swal("Poof! Your record has been deleted!", {
                                icon: "success",

                            });
                        } else {
                            swal("Deleting Data Cancled!!");
                        }
                    });
            })
            $(document).on('click', '#modelBtn', function() {
                var customerId = $(this).data('id');
                $('#customerHidden').val(customerId);
                $('#purchaseModel').modal('show');
            })
            $(document).on('click', '#modelBtnMembership', function() {
                var customerId = $(this).data('id');
                $('#customerHiddenMembership').val(customerId);
                $('#membershipModel').modal('show');
            })
            $(document).on('click', '#modelBtnGift', function() {
                console.log('hello');
                var customerId = $(this).data('id');
                $('#customerHiddenGift').val(customerId);
                $('#giftModel').modal('show');
            })

            $('#giftform').parsley();
            $('#membershipForm').parsley();

            $(document).on('click', '#giftBtn', function() {
                var customer = $('#customerHiddenGift').val();
                var reward = $('#reward_id').val();
                console.log(customer, reward)
                // $.ajax({
                //     type: "post",
                //     url: "{{ route('gifts-check') }}",
                //     data: {
                //         'customer_id': customer,
                //         'reward_id': reward,
                //         '_token': "{{ csrf_token() }}",
                //     },
                //     context: this,
                //     success: function(response) {
                //         console.log(response)
                //         if (response.error) {
                //             toastr["error"](response.error)
                //         }
                //         if (response.success) {
                //             $('#giftform').submit()
                //         }
                //     },
                //     errors: function(error) {

                //     },

                // });
            })
            $(document).on('click', '#membershipBtn', function() {
                var card = $('#card_no').val();
                var customer = $('#customerHiddenMembership').val();
                var membership = $('#membership_id').val();
                console.log(card)
                $.ajax({
                    type: "post",
                    url: "{{ route('membership-check') }}",
                    data: {
                        'card_no': card,
                        'customer_id': customer,
                        'membership_id': membership,
                        '_token': "{{ csrf_token() }}",
                    },
                    // dataType: "dataType",
                    context: this,
                    success: function(response) {
                        console.log(response)
                        if (response.error) {
                            toastr["error"](response.error)
                        }
                        if (response.success) {
                            // alert('dkfjdskf');
                            $('#membershipForm').submit()
                        }
                    },
                    errors: function(error) {

                    },

                });
            })
            
        })
    </script>
@endsection
