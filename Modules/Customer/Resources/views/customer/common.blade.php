{{-- @dd($customer) --}}
<div class="row">
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Full Name <span class="redAstrick">
                    *</span>:</label>
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Full Name', 'required']) !!}


            </div>
            @if ($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Choose Membership<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-8">
                {!! form::select('membership_id', $memberships, null, ['class' => 'form-control', 'id' => 'membership_id', 'placeholder' => 'Choose Membership', 'required']) !!}
            </div>
            @if ($errors->has('membership_id'))
                    <span class="text-danger">{{ $errors->first('membership_id') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Card No:</label>
            <div class="col-sm-8">
                {!! Form::text('card_no', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Card No.']) !!}
            </div>
            @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Email:</label>
            <div class="col-sm-8">
                {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email']) !!}
            </div>
            @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
        </div>
        {{-- <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Aniversary Date:</label>
            <div class="col-sm-8">
                {!! Form::text('aniversary_date', null, ['class' => 'form-control', 'id' => 'aniversary_date', 'placeholder' => 'Aniversary Date']) !!}
            </div>
        </div> --}}
        <div class="form-group row ">
            <label class="col-form-label col-sm-4">Aniversary Date:</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!! Form::text('aniversary_date', null, ['class' => 'form-control', 'id' => 'aniversary_datepicker', 'placeholder' => 'Aniversary Date - mm/dd/yyyy']) !!}
                    {{-- <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="aniversary_datepicker"
                        name="aniversary_date"> --}}
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="ti-calendar"></i></span>
                    </div>
                </div><!-- input-group -->
            </div>
            @if ($errors->has('aniversary_date'))
                    <span class="text-danger">{{ $errors->first('aniversary_date') }}</span>
                @endif
        </div>
        
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Spouse Name:</label>
            <div class="col-sm-8">
                {!! Form::text('spouse_name', null, ['class' => 'form-control', 'id' => 'spouse_name', 'placeholder' => 'Spouse Name']) !!}
            </div>
            @if ($errors->has('spouse_name'))
                    <span class="text-danger">{{ $errors->first('spouse_name') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Phone:</label>
            <div class="col-sm-8">
                {!! Form::text('mobile', null, ['class' => 'form-control', 'id' => 'mobile', 'placeholder' => 'Phone', 'data-parsley-trigger' => 'keyup', 'data-parsley-type' => 'number']) !!}
            </div>@if ($errors->has('mobile'))
            <span class="text-danger">{{ $errors->first('mobile') }}</span>
        @endif

        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Mobile<span class="redAstrick">
                    *</span>:</label>
            <div class="col-sm-8">
                {!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Mobile', 'data-parsley-trigger' => 'keyup',  'data-parsley-type' => 'number']) !!}
            </div>
            @if ($errors->has('phone'))
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Address:</label>
            <div class="col-sm-8">
                {!! Form::text('address', null, ['class' => 'form-control', 'id' => 'address', 'placeholder' => 'Address']) !!}
            </div>
            @if ($errors->has('address'))
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Choose Category<span class="redAstrick"> *</span>:</label>
            <div class="col-sm-8">
                {{-- {!! form::select('category_id', $category, null, ['class' => 'form-control', 'id' => 'category_id', 'placeholder' => 'Choose Category', 'required']) !!} --}}
                {!! form::select('category_id[]', $category, null, ['class' => 'select2 select2-multiple js-example-basic-multiple', 'id' => 'category', 'data-placeholder' => 'Choose Category', 'required','multiple'>'multiple','multiple']) !!}
            </div>
            
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Designation:</label>
            <div class="col-sm-8">
                {!! Form::text('designation', null, ['class' => 'form-control', 'id' => 'designation', 'placeholder' => 'Designation']) !!}
            </div>
            @if ($errors->has('designation'))
                    <span class="text-danger">{{ $errors->first('designation') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Birth Date:</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!! Form::text('birth_date', null, ['class' => 'form-control', 'id' => 'birth_datepicker', 'placeholder' => 'Birth Date - mm/dd/yyyy']) !!}
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="ti-calendar"></i></span>
                    </div>
                </div>
            </div>
            @if ($errors->has('birth_date'))
                    <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                @endif
        </div>
        <div class="form-group row ">
            <label class="col-form-label col-sm-4">Added Date:</label>
            <div class="col-sm-8">
                <div class="input-group">
                    {!! Form::text('added_date', null, ['class' => 'form-control', 'id' => 'added_datepicker', 'placeholder' => 'Added Date - mm/dd/yyyy']) !!}
                    {{-- <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="aniversary_datepicker"
                        name="aniversary_date"> --}}
                    <div class="input-group-append">
                        <span class="input-group-text"><i class="ti-calendar"></i></span>
                    </div>
                </div><!-- input-group -->
            </div>
            @if ($errors->has('aniversary_date'))
                    <span class="text-danger">{{ $errors->first('aniversary_date') }}</span>
                @endif
        </div>
        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-4 col-form-label">Organization/Company:</label>
            <div class="col-sm-8">
                {!! Form::text('company', null, ['class' => 'form-control', 'id' => 'designation', 'placeholder' => 'Organization/Company Name']) !!}
            </div>
            @if ($errors->has('company'))
                    <span class="text-danger">{{ $errors->first('company') }}</span>
                @endif
        </div>
        <div class="form-group mb-0 justify-content-end row">
            <div class="col-sm-9">
                <button type="submit" class="btn btn-info waves-effect waves-light float-right">Submit</button>
            </div>
        </div>
    </div>
</div>
