@extends('dashboard::newLayouts.master')
@section('title')
    Customer Details- {{ $customer->name }}
@endsection
@section('content')
    {{-- @dd($customer) --}}
    @php
        $total = 0;
        foreach ($customer->purchases as $purchase) {
            foreach ($purchase->details as $key => $value) {
                # code...
                $total = $total + $value->amount;
            }
        }
        $totalRewards = $customer->rewards->sum('point_worth');
    @endphp
    <div class="container-fluid flex-grow-1 container-p-y">
    <div class="row">
        <div class="col-12">

            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Dashboard</a></li>

                <li class="breadcrumb-item "><a href="{{ route('customers.index') }}">Customers</a></li>
                <li class="breadcrumb-item active">{{ $customer->name }}</li>
            </ol>
        </div>
        <div class="col-12">
            <div class="card-box task-detail">

                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Customer Name</h5>
                        <p> {{ $customer->name }} <small class="text-muted"></small></p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Mobile No.</h5>
                        <p> {{ !is_null( $customer->phone)?$customer->phone:null }} <small class="text-muted"></small></p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Email</h5>
                        <p>{{ !is_null( $customer->email)?$customer->email:null }}</p>
                    </div>

                </div>
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Address</h5>
                        <p> {{ !is_null( $customer->address)?$customer->address:null }}
                            <small class="text-muted"></small>
                        </p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Aniversary Date</h5>
                        <p>{{ !is_null( $customer->aniversary_date)?Carbon\Carbon::parse($customer->aniversary_date)->format('Y-m-d') :null}} <small class="text-muted"></small>
                        </p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Spouse Name</h5>
                        <p> {{!is_null( $customer->spouse_name)? $customer->spouse_name:null }} <small class="text-muted">g</small></p>
                    </div>
                </div>
                <div class="row task-dates mb-0 mt-2">
                    {{-- <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Card No.</h5>
                        <p> {{ !is_null( $customer->address)?$customer->address:null }}
                            <small class="text-muted"></small>
                        </p> --}}
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Added Date</h5>
                        <p>{{ !is_null( $customer->aniversary_date)?Carbon\Carbon::parse($customer->aniversary_date)->format('Y-m-d') :null}} <small class="text-muted"></small>
                        </p>
                    </div>
                </div>
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Birth Date</h5>
                        <p> {{ !is_null( $customer->birth_date)?Carbon\Carbon::parse($customer->birth_date)->format('Y-m-d') :null}}
                            <small class="text-muted"></small>
                        </p>
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Phone No.</h5>
                        <p> {{ !is_null( $customer->mobile)?$customer->mobile:null }}</p>
                    </div>
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Designation</h5>
                        <p>{{ !is_null( $customer->designation)?$customer->designation:null }}</p>
                    </div>
                </div>
                <div class="row task-dates mb-0 mt-2">
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Rewards</h5>
                        <ul>
                            @foreach ($customer->rewards as $reward)
                                <li>{{$reward->name}}({{$reward->pivot->point_amt}})</li>
                            @endforeach
                        </ul>
                        {{-- <p> {{ $customer->customerDetails->birth_date }}
                            <small class="text-muted"></small>
                        </p> --}}
                    </div>

                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Total Amount Spent</h5>
                        <p> Rs.{{ $total }}</p>
                    </div>
                    @if (!is_null($customer->card))
                        
                    <div class="col-lg-4">
                        <h5 class="font-600 m-b-5">Rewards Point</h5>
                        <p>{{number_format(($customer->card->rewards_point))}}</p>
                    </div>
                    @endif
                </div>

                <hr>
                <div class="form-group mb-0 justify-content-end row">
                    <div class="col-sm-9">
                        <a href="{{ route('customers.index') }}"
                            class="btn btn-info waves-effect waves-light float-right">Back</a>
                        <a href="{{ route('customers.edit', $customer->id) }}"
                            class="btn btn-warning waves-effect waves-light float-right mr-2"
                            style="color:black;">Update</a>
                    </div>
                </div>

            </div>
        </div><!-- end col -->

    </div>
    </div>
    <!-- end row -->
@endsection
