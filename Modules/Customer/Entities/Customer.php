<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Customer\Entities\CustomerDetail;
use Modules\Inventory\Entities\Purchase;
use Modules\Inventory\Entities\Card;
use Modules\Inventory\Entities\Reward;
use Modules\Marketing\Entities\ClientCategory;
class Customer extends Model
{
    // use HasFactory;

    protected $fillable = ['name','email','phone','address','aniversary_date','spouse_name','mobile','birth_date','designation','added_date'];
    
    // protected static function newFactory()
    // {
    //     return \Modules\Customer\Database\factories\CustomerFactory::new();
    // }
    public function customerDetails(){
        return $this->hasOne(CustomerDetail::class,'customer_id');
    }
    public function card(){
        return $this->hasOne(Card::class,'customer_id');
    }
    public function purchases(){
        return $this->hasMany(Purchase::class,'customer_id');
    }
    public function rewards(){
        return $this->belongsToMany(Reward::class,'customer_reward','customer_id','reward_id')->withPivot('point_amt');
    }
    public function clientCategories()
    {
        return $this->belongsToMany(ClientCategory::class, 'accountuser_client_category', 'accountuser_id', 'client_category_id');
    }
}
