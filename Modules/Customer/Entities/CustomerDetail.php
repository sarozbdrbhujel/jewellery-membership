<?php

namespace Modules\Customer\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\Customer\Entities\Customer;
class CustomerDetail extends Model
{

    protected $fillable = ['customer_id','aniversary_id','spouse_name','birth_date','status','mobile','designation','other_informations'];

    public function customer(){
        return $this->hasOne(Customer::class,'customer_id');
    }
}
