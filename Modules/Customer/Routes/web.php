<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>'customer','middleware'=>['auth']],function() {
    route::resource('customers','CustomerController');
    // route::post('imports','CustomerController@uploadUsers')->name('imports');
    route::post('customers/filters','CustomerController@filter')->name('customers-filter');
    Route::post('/priceRanger', 'CustomerController@priceRanger')->name('priceRanger');
    route::post('imports','CustomerController@uploadUsers')->name('imports');
    route::get('imports','CustomerController@import')->name('imports.get');
    // Route::get('/', 'CustomerController@index');
});
