<?php
namespace Modules\Customer\Repositories;
use Modules\Customer\Entities\CustomerDetail;
use Modules\Customer\Interfaces\CustomerDetailsInterface;

class CustomerDetailRepository implements CustomerDetailsInterface{
    public function __construct(CustomerDetail $customerDetail)
    {
        $this->customerDetail = $customerDetail;
    }

    public function getAll($orderBy = null)
    {
        return $this->customerDetail->get();
    }

    public function findOrFail($id)
    {
        return $this->customerDetail->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->customerDetail->find($id);
        return $this->customerDetail->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->customerDetail->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->customerDetail->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->customerDetail->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->customerDetail->whereHas($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->customerDetail->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->customerDetail->pluck($id, $name);
    }


}