<?php
namespace Modules\Customer\Repositories;
use Modules\Customer\Entities\Customer;
use Modules\Customer\Interfaces\CustomerInterface;

class CustomerRepository implements CustomerInterface{
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function getAll($orderBy = null)
    {
        return $this->customer->get();
    }

    public function findOrFail($id)
    {
        return $this->customer->findOrFail($id);
    }
    public function delete($id)
    {
        $model = $this->customer->find($id);
        return $this->customer->find($id)->delete();
    }
    public function store($request = [])
    {
        return  $this->customer->create($request);
    }
    public function update($request = [], $id)
    {
        $model = $this->findOrFail($id);

        $model->update($request);
        return true;

    }

    public function with($attribute, \Closure $closure = null)
    {
       return  $this->customer->with($attribute);
    }
    public function where($attribute, \Closure $closure = null)
    {
       return  $this->customer->where($attribute);
    }


    public function whereHas($attribute, \Closure $closure = null)
    {
        $this->customer->whereHas($attribute, $closure);
    }
    public function doesntHave($attribute, \Closure $closure = null)
    {
        $this->customer->doesntHave($attribute, $closure);
    }
    public function has($attribute, \Closure $closure = null)
    {
        $this->customer->has($attribute, $closure);
    }

    public function pluck($id, $name)
    {
       return $this->customer->pluck($id, $name);
    }


}