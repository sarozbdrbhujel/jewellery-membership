<?php

namespace Modules\Customer\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $data=[
            'name'=>'System Admin',
            'email'=>'admin@admin.com',
            'password' => bcrypt('123456'),
            // 'initial_password_update_status' => false,
        ];
        if(User::get()->isEmpty()){
            $user=User::create($data);
        }
        // $this->call("OthersTableSeeder");
    }
}
