<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Customer\Http\Requests\CustomerRequest;
use Modules\Customer\Interfaces\CustomerInterface;
use Modules\Customer\Interfaces\CustomerDetailsInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\Customer\Entities\Customer;
use Modules\Inventory\Interfaces\MembershipInterface;
use Modules\Inventory\Interfaces\RewardInterface;
use Modules\Inventory\Interfaces\CardInterface;
use Maatwebsite\Excel\Facades\Excel;
// use Excel;
use App\Imports\CustomerImport;
use Carbon\Carbon;
use Modules\Marketing\Enums\MarketingMessageEnum;
use Modules\Marketing\Entities\Message;
use Modules\Marketing\Emails\BirthdayMail;
use Mail;
use Modules\Marketing\Entities\ClientCategory;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function __construct(CustomerInterface $customerInterface, CustomerDetailsInterface $customerDetailsInterface, MembershipInterface $membershipInterface, RewardInterface $rewardInterface, CardInterface $cardInterface,ClientCategory $clientCategory)
    {
        $this->customerInterface = $customerInterface;
        $this->customerDetailsInterface = $customerDetailsInterface;
        $this->membershipInterface = $membershipInterface;
        $this->rewardInterface = $rewardInterface;
        $this->cardInterface = $cardInterface;
        $this->clientCategory =$clientCategory;
    }

    public function index()
    {
        $data = [
            'from_date' => null,
            'to_date' => null,
            // 'member_no' =>  null,
            'card_type' =>  null,
            'customer_id' =>  null,
        ];
        $data['cusotmers'] = $this->customerInterface->with(['card.membership', 'purchases.details', 'rewards' => function ($q) {
            $q->orderBy('created_at', 'desc');
        }])->paginate(10);
        $data['cust'] = $this->customerInterface->pluck('name', 'id');
        // $data['cusotmers']=Customer::withSum('purchases','amount')->get();
        $data['memberships'] = $this->membershipInterface->pluck('name', 'id');
        $data['rewards'] = $this->rewardInterface->pluck('name', 'id');
        return view('customer::customer.index', $data);
    }
    public function import()
    {   
        
        // $customerbirth=$this->customerInterface->with(['customerDetails'])->where('birth_date', Carbon::today()->format('m/d/Y'))->get();;
        // dd($customerbirth);
        return view('customer::customer.import');
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        $data['memberships'] = $this->membershipInterface->pluck('name', 'id');
        $data['category'] = $this->clientCategory->pluck('category_name', 'id');
        return view('customer::customer.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(CustomerRequest $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $customerData = [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'aniversary_date' => $request->aniversary_date,
                'spouse_name' => $request->spouse_name,
                'mobile' => $request->mobile,
                'birth_date' => $request->birth_date,
                'designation' => $request->designation,
                'added_date' => $request->added_date,
            ];
            $customer = $this->customerInterface->store($customerData);
            $cardData = [
                'membership_id' => $request->membership_id,
                'customer_id' => $customer->id,
                'card_no' => $request->card_no,
            ];
            $card = $this->cardInterface->store($cardData);
            // dd(   $card,$customer, $cardData);
            if ($customer) {

                $customerDetailsData = [
                    'aniversary_date' => $request->aniversary_date,
                    'spouse_name' => $request->spouse_name,
                    'mobile' => $request->mobile,
                    'birth_date' => $request->birth_date,
                    'designation' => $request->designation,
                    'customer_id' => $customer->id,

                ];
                
                    // foreach($request->category_id as $cat){

                    //     $category = $this->clientCategory->findOrFail(  $cat);
                    //             $category->accountUsers()->sync($customer->id);
                    // }
                    $customer->clientCategories()->sync($request->category_id);
                $cusomerDetail = $this->customerDetailsInterface->store($customerDetailsData);

            }
            if($customer && !is_null($customer->email)){

                $welcomemail=Message::where('message_type',MarketingMessageEnum::EmailWelcomeWish)->first();
                $custmoEmail = new BirthdayMail($welcomemail->subject,$welcomemail->message,$customer->name);
                // dd($custmoEmail,$cus->welcomemail);
                Mail::to($customer->email)->send($custmoEmail);
            }
        } catch (Exception $e) {
            DB::rollback();
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('customers.index')->with('message', 'New Customer Created Successfully');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
        $data['customer'] = $this->customerInterface->with(['customerDetails', 'card.membership', 'purchases', 'rewards'])->findOrFail($id);
        return view('customer::customer.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        $data['customer'] = $this->customerInterface->with(['customerDetails', 'card.membership', 'purchases', 'rewards','clientCategories'])->findOrFail($id);
        // dd( $data['customer']);
        $data['memberships'] = $this->membershipInterface->pluck('name', 'id');
        $data['category'] = $this->clientCategory->pluck('category_name', 'id');
        return view('customer::customer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $cusomerDetails = $this->customerDetailsInterface->where(['customer_id' => $id])->first();
            $customerData = [
                'name' => $request->name,
                'email' => $request->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'aniversary_date' => $request->aniversary_date,
                'spouse_name' => $request->spouse_name,
                'mobile' => $request->mobile,
                'birth_date' => $request->birth_date,
                'designation' => $request->designation,
                'added_date' => $request->added_date,
            ];
            $customer = $this->customerInterface->with('card')->update($customerData, $id);
            $custss = $this->customerInterface->with(['card'])->find($id);
           
            $custss->card->update(['membership_id' => $request->membership_id,
            'card_no' => $request->card_no
                ]);
            
                // dd( $custss);
            // if ($cusomerDetails) {

            //     $customerDetailsData = [
            //         'aniversary_date' => $request->aniversary_date,
            //         'spouse_name' => $request->spouse_name,
            //         'mobile' => $request->mobile,
            //         'birth_date' => $request->birth_date,
            //         'designation' => $request->designation,

            //     ];
            //     $cusomerDetail = $this->customerDetailsInterface->update($customerDetailsData, $cusomerDetails->id);
            // }
            $custss->clientCategories()->sync($request->category_id);
        } catch (Exception $e) {
            DB::rollback();
            return response($e->getMessage());
        }
        DB::commit();
        return redirect()->route('customers.index')->with('message', 'Customer Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        try {
            $customerDetail = $this->customerDetailsInterface->where(['customer_id' => $id])->first();
            $this->customerDetailsInterface->delete($customerDetail->id);
            $this->customerInterface->delete($id);
        } catch (Exception $e) {
            return response($e->getMessage());
        }
        return redirect()->route('customers.index')->with('message', 'Customer Deleted Successfully');
    }
    public function filter(Request $request)
    {
        // dd($request->all());
        $data = [
            'from_date' => $request->from_date,
            'to_date' => $request->to_date,
            // 'member_no' =>  $request->member_no,
            'customer_id' => $request->customer_id,
            'card_type' =>  $request->card_type,
        ];
        $data['memberships'] = $this->membershipInterface->pluck('name', 'id');
        $data['rewards'] = $this->rewardInterface->pluck('name', 'id');
        $data['cust'] = $this->customerInterface->pluck('name', 'id');
        $data['cusotmers'] = $this->customerInterface
            // ->with(['productItem.product','productItem.metal','productItem.productItemDetails'])
            ->with(['card.membership', 'purchases', 'rewards'])
            ->when($request->card_type, function ($query) use ($request) {
                $query
                    ->whereHas('card.membership', function ($query) use ($request) {
                        $query->where('id',  $request->card_type);
                    });
            })
            ->when($request->from_date && $request->to_date, function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('created_at', '>=', $request->from_date)
                        ->where('created_at', '<=', $request->to_date);
                });
            })
            ->when(!($request->from_date) && $request->to_date, function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('created_at', '<=', $request->to_date);
                });
            })
            ->when($request->from_date && !($request->to_date), function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('created_at', '>=', $request->from_date);
                });
            })
            // ->whereHas('stockeffect', function($query){

            //     $query->where('stock_effect_name', StockEnum::Sales);
            // })
            ->when($request->customer_id, function ($query) use ($request) {
                // $query->whereHas('productItem.metal' , function($query)use($request){
                $query->where('id', $request->customer_id);
                // });
            })

            // ->when($request->product_id , function($query) use($request){
            //     $query->whereHas('productItem.product' , function($query)use($request){
            //         $query->where('id',  $request->product_id);
            //     });
            // })
            ->get();
        return view('customer::customer.index', $data);
    }
    public function uploadUsers(Request $request)
    {
        
        // dd($request->file);
        try {
        Excel::import(new CustomerImport, $request->file);
        }catch (Exception $e) {
            // DB::rollback();
            // dd('sdkfjsd');
            return response($e->getMessage());
        }

        return redirect()->route('customers.index')->with('success', 'User Imported Successfully');
    }
    public function priceRanger(Request $request){
        dd($request->all());
        $min=$request->min;
        $max=$request->max;
    }
}
