<?php

namespace Modules\Customer\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required',
            'email'=>'sometimes|nullable|email|unique:customers,email,'.$this->customer,
            'phone'=>'sometimes|nullable|min:9|max:14|regex:/[0-9]{9}/|unique:customers,phone,'.$this->customer,
            'mobile'=>'sometimes|nullable|min:9|max:14|regex:/[0-9]{9}/',
            // 'email'=>'some',
            'membership_id'=>'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
