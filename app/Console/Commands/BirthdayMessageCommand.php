<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Customer\Entities\Customer;
use Carbon\Carbon;
use Modules\Marketing\Entities\Message;
use Modules\Marketing\Enums\MarketingMessageEnum;
use Modules\Marketing\Emails\BirthdayMail;
use Mail;
class BirthdayMessageCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthdaymessage:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        
        $birthdaycustomer=Customer::where('birth_date',Carbon::today()->format('m/d/Y'))->whereNotNull('email')->get();
        $aviverseryCustomer=Customer::where('aniversary_date',Carbon::today()->format('m/d/Y'))->whereNotNull('email')->get();
        $birthdayemail=Message::where('message_type',MarketingMessageEnum::EmailBirthdayWish)->first();
        $aniverseryemail=Message::where('message_type',MarketingMessageEnum::EmailAnniversarydayWish)->first();
        // return view('marketing::birthdayemails.birthdayemailMessages')->with(['name'=>'saroj','message'=>$birthdayemail->message]);
        // dd($birthdayemail);
        if($birthdaycustomer->isNotEmpty()){
            foreach ($birthdaycustomer as $cus)
            {
                
                
                $custmoEmail = new BirthdayMail($birthdayemail->subject,$birthdayemail->message,$cus->name);
                // dd($custmoEmail,$cus->birthdayemail);
                Mail::to($cus->email)->send($custmoEmail);
                
                // $custmoEmail = new NotificationEmail($this->fromAddress, $this->template, $this->subject, $this->username);
                // Mail::to($cus->birthdayemail)->send($custmoEmail);
            }
        }
        if($aviverseryCustomer->isNotEmpty()){
            foreach ($birthdaycustomer as $cus)
            {
                
                
                $custmoEmail = new BirthdayMail($aniverseryemail->subject,$aniverseryemail->message,$cus->name);
                // dd($custmoEmail,$cus->birthdayemail);
                Mail::to($cus->email)->send($custmoEmail);
                
                // $custmoEmail = new NotificationEmail($this->fromAddress, $this->template, $this->subject, $this->username);
                // Mail::to($cus->birthdayemail)->send($custmoEmail);
            }
        }
        \Log::info("Cron is working fine!");
    }
}
