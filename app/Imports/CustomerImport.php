<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Modules\Customer\Entities\Customer;
use Modules\Inventory\Entities\Card;
use Modules\Inventory\Entities\PurchaseDetails;
use Modules\Inventory\Entities\Purchase;
use Exception;
use Illuminate\Support\Facades\DB;
use Modules\Inventory\Helpers\TransferDateHelper;
class CustomerImport implements ToCollection,WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $rows)
    {
        try{
            DB::beginTransaction() ;
            // dd($rows[2]);
            foreach ($rows as $row) 
            {
                // dd($row);
                if(!is_null($row['first_name'])){
                    // dd($row['membership']);
                    
                    $customer= Customer::create([
                       
                        "name"=>(!is_null($row['first_name'])?$row['first_name']:null).' '.(!is_null($row['middle_name'])?$row['middle_name']:null).' '.(!is_null($row['last_name'])?$row['last_name']:null),
                        "added_date"=>!is_null($row['date_added'])?TransferDateHelper::transformDate($row['date_added']):null,
                        "email"=>!is_null($row['email'])?$row['email']:null,
                        "phone"=>!is_null($row['mobile_no'])?$row['mobile_no']:null,
                        "address"=>!is_null($row['address'])?$row['address']:null,
                        "aniversary_date"=>!is_null($row['anniversary_date'])?TransferDateHelper::transformDate($row['anniversary_date']):null,
                        "spouse_name"=>!is_null($row['spouse_name'])?$row['spouse_name']:null,
                        "birth_date"=>!is_null($row['birth_date'])?TransferDateHelper::transformDate($row['birth_date']):null,
                       
                    ]);
                    // dd($customer);
                    if($row['membership']='Padmini'){
                        $member_id=1;
                    }
                    if($row['membership']='Suryakanti'){
                        $member_id=2;
                    }
                        if($row['membership']='Madhavi'){
                            $member_id=3;
                        }
                   $card=Card::create([
                        "card_no"=>!is_null($row['card_number'])?$row['card_number']:null,
                        "customer_id"=>$customer->id,
                        "authorized_by"=>1,
                        "membership_id"=> $member_id,
                    ]);
                    // dd($customer,$card);
                    $purchase=Purchase::create([
                        'customer_id'=>$customer->id,
                    ]);
                   $PurchaseDetails= PurchaseDetails::create([
                        'purchase_id'=>$purchase->id,
                        'amount'=>!is_null($row['total_purchase'])?$row['total_purchase']:0,
                        'bill_nos'=>0000,
                    ]);
                    // dd($customer,$card,$purchase,$PurchaseDetails);
                    
                }
            }
        }catch(Exception $e){

            DB::rollback();
            dd($e->getMessage());
            // return response($e->getMessage());
        }
        DB::commit();
    }
    public function model(array $row)
    {
        // if(!empty($row['first_name'])){

        //     return new Customer([
        //         // "first_name" => $row['first_name'],
        //         // "last_name" => $row['last_name'],
        //         // "email" => $row['email'],
        //         // "mobile_number" => $row['mobile_number'],
        //         // "role_id" => 2, // User Type User
        //         // "status" => 1,
        //         // "password" => Hash::make('password'),
        //         // "name');
        //         "name"=>!is_null($row['first_name'])?$row['first_name']:null,
        //         "email"=>!is_null($row['email'])?$row['email']:null,
        //         "phone"=>!is_null($row['mobile_no'])?$row['mobile_no']:null,
        //         "mobile"=>!is_null($row['mobile_no'])?$row['mobile_no']:null,
        //         "address"=>!is_null($row['address'])?$row['address']:null,
        //         "aniversary_date"=>!is_null($row['anniversary_date'])?$row['anniversary_date']:null,
        //         "spouse_name"=>!is_null($row['spouse_name'])?$row['spouse_name']:null,
        //         "birth_date"=>!is_null($row['birth_date'])?$row['birth_date']:null,
        //         // "name"=>!is_null($row[3])?$row[3]:null,
        //         // "email"=>!is_null($row[12])?$row[12]:null,
        //         // "phone"=>!is_null($row[11])?$row[11]:null,
        //         // "mobile"=>!is_null($row[11])?$row[11]:null,
        //         // "address"=>!is_null($row[8])?$row[8]:null,
        //         // "aniversary_date"=>!is_null($row[10])?$row[10]:null,
        //         // "spouse_name"=>!is_null($row[6])?$row[6]:null,
        //         // "birth_date"=>!is_null($row[9])?$row[9]:null,
        //         // "designation"=>!is_null($row[designation])?$row[designation]:null,
        //         // "other_informations"=>!is_null($row[other_informations])?$row[other_informations]:null,
        //     ]);
        // }
    }
}
