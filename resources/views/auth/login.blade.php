<!DOCTYPE html>

<html lang="en" class="material-style layout-fixed">

<head>
    <title>Ganapati Jewellers || Membership</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="Empire Bootstrap admin template made using Bootstrap 4, it has tons of ready made feature, UI components, pages which completely fulfills any dashboard needs." />
    <meta name="keywords" content="Empire, bootstrap admin template, bootstrap admin panel, bootstrap 4 admin template, admin template">
    <meta name="author" content="Srthemesvilla" />
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <!-- Icon fonts -->
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/fontawesome.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/ionicons.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/linearicons.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/open-iconic.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/pe-icon-7-stroke.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/fonts/feather.css')}}">

    <!-- Core stylesheets -->
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/bootstrap-material.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/shreerang-material.css')}}">
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/uikit.css')}}">

    <!-- Libs -->
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/libs/perfect-scrollbar/perfect-scrollbar.css')}}">
    <!-- Page -->
    <link rel="stylesheet" href="{{Module::asset('dashboard:assets/css/pages/authentication.css')}}">
</head>

<body>
    <!-- [ Preloader ] Start -->
    <div class="page-loader">
        <div class="bg-primary"></div>
    </div>
    <!-- [ Preloader ] End -->

    <!-- [ content ] Start -->
    <div class="authentication-wrapper authentication-1 px-4">
        <div class="authentication-inner py-5">

            <!-- [ Logo ] Start -->
            <div class="d-flex justify-content-center align-items-center">
                <div class="">
                    <div class="w-100 position-relative">
                        <img src="{{Module::asset('dashboard:images/logo.png')}}" alt="Brand Logo" class="img-fluid">
                    </div>
                </div>
            </div>
            <!-- [ Logo ] End -->

            <!-- [ Form ] Start -->
            {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}
            <form class="my-5" method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <label class="form-label">Email</label>
                    <input type="text" class="form-control" name="email" @error('email') is-invalid @enderror">
                    <div class="clearfix"></div>
                   @if ($errors->has('email'))
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                @endif
                </div>
                <div class="form-group">
                    <label class="form-label d-flex justify-content-between align-items-end">
                        <span>Password</span>
                        {{-- <a href="pages_authentication_password-reset.html" class="d-block small">Forgot password?</a> --}}
                    </label>
                    <input type="password" class="form-control" @error('password') is-invalid @enderror" name="password">
                     @if ($errors->has('password'))
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                @endif
                    <div class="clearfix"></div>
                </div>
                <div class="d-flex justify-content-between align-items-center m-0">
                    <label class="custom-control custom-checkbox m-0">
                        <input type="checkbox" class="custom-control-input" >
                        <span class="custom-control-label">Remember me</span>
                    </label>
                    
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </form>
            <!-- [ Form ] End -->

            <div class="text-center text-muted">
                Don't have an account yet?
                <a href="pages_authentication_register-v1.html">Sign Up</a>
            </div>

        </div>
    </div>
    <!-- [ content ] End -->

    <!-- Core scripts -->
    <script src="{{Module::asset('dashboard:assets/js/pace.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/libs/popper/popper.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/js/bootstrap.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/js/sidenav.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/js/layout-helpers.js')}}"></script>
    <script src="{{Module::asset('dashboard:assets/js/material-ripple.js')}}"></script>

    <!-- Libs -->
    <script src="{{Module::asset('dashboard:assets/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

    <!-- Demo -->
    {{-- <script src="{{Module::asset('dashboard:assets/js/demo.js')}}"> --}}
    </script><script src="{{Module::asset('dashboard:assets/js/analytics.js')}}"></script>
</body>

</html>
